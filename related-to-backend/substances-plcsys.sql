SELECT s.sub_id,
       s.naml,
       s.ec,
       s.cas,
       s.idx,
       esp.entr_id,
       esp.entr_tp,
       plc1.plc_id,
       plc1.name,
       plc1.status,
       plc1.tp,
       epp1.entr_id,
       plcsys1.plcsys_id,
       plcsys1.plcsys_ke,
       plcsys1.tp,
       epp2.entr_id,
       plcsys2.plcsys_id,
       plcsys2.plcsys_ke,
       plcsys2.tp


FROM substances s
         LEFT JOIN entr_substances_plc esp ON s.sub_id = esp.entr_src_id
         INNER JOIN plc plc1 ON plc1.plc_id = esp.entr_dst_id AND plc1.ns = 'EcoMundo'
    AND esp.entr_tp LIKE ANY (VALUES ('CLP_H%'), ('CLP_S%'), ('DSD_S%'), ('DSD_H%'))
         LEFT JOIN entr_plc_plcsys epp1 ON plc1.plc_id = epp1.entr_src_id AND
                                           epp1.entr_tp IN ('HAZARD_CLASS_CAT', 'H_STMT_SUPP', 'RISK', 'SAFETY')
         INNER JOIN plcsys plcsys1 ON plcsys1.plcsys_id = epp1.entr_dst_id
         LEFT JOIN entr_plcsys_plcsys epp2 ON plcsys1.plcsys_id = epp2.entr_src_id
         INNER JOIN plcsys plcsys2 ON plcsys2.plcsys_id = epp2.entr_dst_id
WHERE s.sub_id = 2641231