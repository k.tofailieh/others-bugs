-- query 1:
SELECT DISTINCT(l_size.lov_id)               AS lov_size_id,
               'LABEL_SIZE'                  AS entity_tp,
               l_size.ke                     AS name,
               sdsdoc.lg                     AS language,
               (SELECT count(1)
                FROM lov
                         INNER JOIN entr_sdsdoc_lov esl1
                                    ON esl1.entr_src_id = sdsdoc.sdsdoc_id AND esl1.entr_tp = 'LABEL_TYPE'
                WHERE lov.tp = 'LABEL_TYPE') AS child_total,
               2                             AS level,
               -- :parentId                     AS entity_id,
               'fa fa-tags'                  AS icon_cls
FROM sdsdoc
         INNER JOIN entr_sdsdoc_lov esl2 ON esl2.entr_src_id = sdsdoc.sdsdoc_id AND esl2.entr_tp = 'LABEL_SIZE'
         INNER JOIN lov l_size ON l_size.lov_id = esl2.entr_dst_id
         INNER JOIN entr_fiche_sdsdoc efs
                    ON efs.entr_dst_id = sdsdoc.sdsdoc_id AND efs.entr_tp IN ('PCK_LABEL', 'SKU_LABEL')
     --  AND efs.entr_src_id = :parentId
WHERE sdsdoc.tp IN ('PCK_LABEL', 'SKU_LABEL')

GROUP BY l_size.lov_id, sdsdoc.lg, sdsdoc.sdsdoc_id;

------------------------------------------------------------------------------------------------------------------------
-- query 2:
SELECT DISTINCT l_type.lov_id                    AS entity_id,
                'LABEL_TYPE'                     AS entity_tp,
                mat.m_id,
                l_type.lov_id,
                l_type.ke                        AS name,
                :lovSizeId                       AS lov_size_id,
                (SELECT count(1)
                 FROM sdsdoc
                          INNER JOIN entr_sdsdoc_lov esl1
                                     ON esl1.entr_src_id = sdsdoc.sdsdoc_id AND esl1.entr_tp = 'LABEL_TYPE'
                 WHERE l_type.tp = 'LABEL_TYPE') AS child_total,
                3                                AS level,
                'fa fa-tags'                     AS icon_cls
FROM sdsdoc
         INNER JOIN entr_sdsdoc_lov esl ON esl.entr_src_id = sdsdoc.sdsdoc_id AND esl.entr_tp = 'LABEL_TYPE'
         INNER JOIN lov l_type ON l_type.lov_id = esl.entr_dst_id
         INNER JOIN entr_fiche_sdsdoc efs2 ON efs2.entr_dst_id = sdsdoc.sdsdoc_id AND efs2.entr_tp = 'PCK_LABEL'
         INNER JOIN fiche mat ON mat.m_id = efs2.entr_src_id AND mat.m_id = :parentId

         INNER JOIN entr_sdsdoc_lov esl3 ON
            esl3.entr_src_id = sdsdoc.sdsdoc_id
        AND esl3.entr_tp = 'LABEL_SIZE' AND esl3.entr_dst_id = :lovSizeId
WHERE sdsdoc.tp IN ('PCK_LABEL', 'SKU_LABEL')

ORDER BY l_type.lov_id DESC;

------------------------------------------------------------------------------------------------------------------------

-- query 3:

SELECT DISTINCT sdsdoc.sdsdoc_id   AS entity_id,
                'Sdsdoc'           AS entity_tp,
                mat.m_id,
                l_type.ke,
                sdsdoc.sds_version AS name,
                sdsdoc.name        AS label_name,
                sdsdoc.cr_dt       AS creationDate,
                l_type.lov_id,
                --:parentId          AS parent_id,
               -- :lovSizeId         AS lov_size_id,
                'fa fa-tags'       AS icon_cls,
                '{' ||
                string_agg('"(' || dir_id || ',' || coalesce(cn, '') || ',' || coalesce(email, '') || ')"', ',') ||
                '}'                AS assignees
FROM sdsdoc
         LEFT JOIN entr_project_sdsdoc eps ON eps.entr_dst_id = sdsdoc.sdsdoc_id AND eps.entr_tp = 'TASK_LABEL_'
         LEFT JOIN project ON project.project_id = eps.entr_src_id
         LEFT JOIN entr_project_dir epd ON epd.entr_src_id = project.project_id AND epd.entr_tp = 'ASSIGNEE'
         LEFT JOIN dir ON dir.dir_id = epd.entr_dst_id
         INNER JOIN entr_sdsdoc_lov esl ON esl.entr_src_id = sdsdoc.sdsdoc_id AND esl.entr_tp = 'LABEL_TYPE'
         INNER JOIN lov l_type ON l_type.lov_id = esl.entr_dst_id
         INNER JOIN entr_sdsdoc_lov esl2 ON esl2.entr_src_id = sdsdoc.sdsdoc_id AND esl2.entr_tp = 'LABEL_SIZE'
         INNER JOIN lov l_size ON l_size.lov_id = esl2.entr_dst_id
         INNER JOIN entr_fiche_sdsdoc efs
                    ON efs.entr_dst_id = sdsdoc.sdsdoc_id AND efs.entr_tp IN ('PCK_LABEL', 'SKU_LABEL')
         INNER JOIN fiche mat ON mat.m_id = efs.entr_src_id
WHERE sdsdoc.tp IN ('PCK_LABEL', 'SKU_LABEL')
  AND mat.m_id = :mId


GROUP BY l_type.lov_id, mat.m_id, sdsdoc.sds_version, sdsdoc.name, sdsdoc.cr_dt, sdsdoc.sdsdoc_id
ORDER BY sdsdoc.sdsdoc_id DESC;
