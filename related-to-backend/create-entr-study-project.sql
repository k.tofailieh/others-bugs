/* --------- Table entr_project_study ------------- */
CREATE TABLE IF NOT EXISTS entr_project_study
(
  entr_id bigint NOT NULL,
  entr_src_id bigint NOT NULL,
  entr_dst_id bigint NOT NULL,
  entr_tp character varying(250),
  entr_co character varying(4000),
  entr_nt character varying(4000),
  entr_st character varying(700),
  entr_num1 bigint,
  entr_num2 bigint,
  entr_char1 character varying(4000),
  entr_char2 character varying(4000),
  entr_float1 double precision,
  entr_float2 double precision,
  entr_xml text,
  entr_dt1 TIMESTAMP WITHOUT TIME ZONE,
  entr_dt2 TIMESTAMP WITHOUT TIME ZONE,
  tpsfrom TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT entr_project_study_pkey PRIMARY KEY (entr_id),
  CONSTRAINT fk_entr_project_study_src FOREIGN KEY (entr_src_id) REFERENCES project (project_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_entr_project_study_dst FOREIGN KEY (entr_dst_id) REFERENCES study (study_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION
)
WITH (OIDS=FALSE);

COMMENT ON COLUMN entr_project_study.tpsfrom IS '';
COMMENT ON COLUMN entr_project_study.entr_tp IS '';
COMMENT ON TABLE  entr_project_study IS '';

CREATE SEQUENCE IF NOT EXISTS  seq_entr_project_study_id;
CREATE INDEX IF NOT EXISTS i_entr_project_study_entr_tp  ON entr_project_study  USING btree  (entr_tp);
CREATE INDEX IF NOT EXISTS i_entr_project_study_entr_dst_id  ON entr_project_study  USING btree  (entr_dst_id);
CREATE UNIQUE INDEX IF NOT EXISTS u_entr_project_study_all  ON entr_project_study  USING btree  (entr_src_id, entr_dst_id, entr_tp);

SELECT tps.tps_decor_table('public.entr_project_study');

/*
######  ######  #######
#     # #     #    #    #####   ####
#     # #     #    #    #    # #
######  #     #    #    #    #  ####
#     # #     #    #    #####       #
#     # #     #    #    #      #    #
######  ######     #    #       ####
*/

SELECT dblink_connect('xmat-tps', 'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$
/* --------- Table entr_project_study ------------- */
CREATE TABLE IF NOT EXISTS entr_project_study
(
  entr_id bigint NOT NULL,
  entr_src_id bigint NOT NULL,
  entr_dst_id bigint NOT NULL,
  entr_tp character varying(250),
  entr_co character varying(4000),
  entr_nt character varying(4000),
  entr_st character varying(700),
  entr_num1 bigint,
  entr_num2 bigint,
  entr_char1 character varying(4000),
  entr_char2 character varying(4000),
  entr_float1 double precision,
  entr_float2 double precision,
  entr_xml text,
  entr_dt1 TIMESTAMP WITHOUT TIME ZONE,
  entr_dt2 TIMESTAMP WITHOUT TIME ZONE,
  tpsfrom TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
  CONSTRAINT entr_project_study_pkey PRIMARY KEY (entr_id)
)
WITH (OIDS=FALSE);

COMMENT ON COLUMN entr_project_study.entr_tp IS '';
COMMENT ON COLUMN entr_project_study.tpsfrom IS '';
COMMENT ON TABLE  entr_project_study IS '';

CREATE SEQUENCE IF NOT EXISTS  seq_entr_project_study_id;
CREATE INDEX IF NOT EXISTS i_entr_project_study_entr_tp  ON entr_project_study  USING btree  (entr_tp);
CREATE INDEX IF NOT EXISTS i_entr_project_study_entr_dst_id  ON entr_project_study  USING btree  (entr_dst_id);
CREATE UNIQUE INDEX IF NOT EXISTS u_entr_project_study_all  ON entr_project_study  USING btree  (entr_src_id, entr_dst_id, entr_tp);

DO $prepare_db_for_tps$ BEGIN PERFORM  tps.tps_decor_table('public.entr_project_study'); END; $prepare_db_for_tps$

$bdlink_exec_containt$);

SELECT dblink_disconnect('xmat-tps');