
/* script to add column tpsfrom into entr_project_study. */

ALTER TABLE public.entr_project_study ADD COLUMN IF NOT EXISTS tpsfrom TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL;


/*
######  ######  #######
#     # #     #    #    #####   ####
#     # #     #    #    #    # #
######  #     #    #    #    #  ####
#     # #     #    #    #####       #
#     # #     #    #    #      #    #
######  ######     #    #       ####
*/

SELECT dblink_connect('xmat-tps', 'dbname=${DB_XMATNAMETPS} port=${DB_PORTTPS} host=${DB_HOSTTPS} user=${DB_USER} password=''${DB_PASSWD}'''::text);

SELECT dblink_exec('xmat-tps', $bdlink_exec_containt$

    ALTER TABLE public.entr_project_study ADD COLUMN IF NOT EXISTS tpsfrom TIMESTAMP(0) WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL;

$bdlink_exec_containt$);

SELECT dblink_disconnect('xmat-tps');