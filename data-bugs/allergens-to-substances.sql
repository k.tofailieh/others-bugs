/*
    Get all information relation to the situation of having no entr_fiche_substances for allergens and ingredients.
*/
WITH Ingredients_without_substance_link AS (SELECT m_id,
                                                   tp,
                                                   ns,
                                                   naml                AS sub_name,
                                                   inci_substances.cas AS sub_cas,
                                                   inciname,
                                                   incicas,
                                                   efi.entr_tp         AS fiche_inci_tp,
                                                   efi.entr_dst_id     AS inci_id,
                                                   substances_count_per_inci,
                                                   inci_substances.sub_id,
                                                   fiche.m_ref
                                            FROM fiche
                                                     INNER JOIN anx.entr_fiche_inci efi
                                                                ON m_id = entr_src_id AND entr_tp IN ('INGREDIENT', 'ALLERGEN')
                                                     INNER JOIN (SELECT entr_dst_id,
                                                                        entr_src_id                                        AS sub_id,
                                                                        naml,
                                                                        substances.cas,
                                                                        inci.inciname,
                                                                        inci.cas                                              incicas,

                                                                        count(entr_src_id) OVER (PARTITION BY entr_dst_id) AS substances_count_per_inci
                                                                 FROM anx.entr_substances_inci
                                                                          INNER JOIN substances ON sub_id = entr_src_id
                                                                          INNER JOIN anx.inci ON entr_substances_inci.entr_dst_id = inci.inci_id
                                                                 WHERE entr_tp = 'INCI_NAME') inci_substances -- subquery to count how many substances related to each INCI.
                                                                USING (entr_dst_id)
                                                     LEFT JOIN entr_fiche_substances efs
                                                               ON fiche.m_id = efs.entr_src_id AND
                                                                  efs.entr_tp IN ('INGREDIENT', 'ALLERGEN')
                                            WHERE efs.entr_id IS NULL -- have no relation entr_fich_substances.
                                              AND fiche.tp IN ('INGREDIENT', 'ALLERGEN'))
INSERT
INTO import.import_line(imp_ref, value_id1, value_id2, value_id3, value1, value2, value3, value4, value5, value6,
                        value7,
                        value8)
SELECT 'fix-entr-fiche-substances',
       m_id,
       sub_id,
       inci_id,
       tp,
       sub_name,
       sub_cas,
       inciname,
       incicas,
       fiche_inci_tp,
       substances_count_per_inci,
       ns
FROM Ingredients_without_substance_link
;
-- 36 record (29 of them related to Alvend).

/* set error for the records that have many substances per INCI with mismatch using name and cas. */
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') ||
                  'Cannot chose the correct substance, the related INCI has many substances with mismatched name and cas. '
WHERE imp_ref = : imp_ref
  AND cast(value7 AS integer) > 1
  AND NOT (upper(trim(value2)) = upper(trim(value4)) AND upper(trim(value5)) = upper(trim(value3)));
-- 5 records will update contains 2 Allergens.


/* insert entr_fiche_substances for the records that have (substance_count_per_inci = 1) */
INSERT INTO entr_fiche_substances(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
SELECT nextval('seq_entr_fiche_substances_id'),
       value_id1,
       value_id2,
       value6,
       'insert entr_fiche_substances to correct the data.'
FROM import.import_line
WHERE imp_ref = :imp_ref           -- 'fix-entr-fiche-substances'
  AND NOT coalesce(imp_err, FALSE) -- one substance or many-substances with one matched by name  and cas.
  AND value_id1 IS NOT NULL
  AND value_id2 IS NOT NULL
-- and upper(trim(value8)) LIKE upper('Alvend%')  filter on ns.
GROUP BY value_id1, value_id2, value6
;
-- 31 will inserted(7 of them have 'EcoMundo%').

/* Extract the cases that cannot correct. */
SELECT row_number() OVER (PARTITION BY value_id1, value_id3),
       value_id1 m_ref,
       value_id2 sub_id,
       value_id3 inci_id,
       value1    fiche_tp,
       value2    sub_name,
       value3    sub_cas,
       value4    inciname,
       value5    incicas,
       value6    fiche_inci_tp,
       value7    substances_count_per_inci,
       value8    ns
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE);
-- 5 records contains 2 Allergens. (all of them for 'Alvend')
