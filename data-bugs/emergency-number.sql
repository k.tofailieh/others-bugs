SELECT DISTINCT p.ke, p2.*
FROM sdschap parentChap
     INNER JOIN entr_sdschap_phractxtke esp
                ON parentChap.sdschap_id = esp.entr_src_id AND entr_tp IN ('HEADING', 'SELECTED')
     INNER JOIN phractxtke p
                ON esp.entr_dst_id = p.phractxtke_id AND UPPER(TRIM(p.ke)) = 'EMERGENCY_TELEPHONE_NUMBER'
     INNER JOIN entr_phractxtke_phractxt epp ON p.phractxtke_id = epp.entr_src_id AND
                                                COALESCE(epp.entr_tp, 'TRANSLATION') = 'TRANSLATION'
     INNER JOIN phractxt p2 ON epp.entr_dst_id = p2.phractxt_id

WHERE parentChap.numero LIKE '01.04_';

SELECT COUNT(*), entr_tp
FROM entr_phractxtke_phractxt
GROUP BY entr_tp;



WITH Emergency_Numbers(Number, Country_Code, Country_Name) AS
         (VALUES ('33 (0)1 45 42 59 59', 'FR', 'France'),
                 ('Open 24 hours a day 0800 147 111 (the call is free of charge) 09 471 977', 'FI', 'Finlande'),
                 ('Poison information telephone number (Mürgistusteabekeskuse number) is nationally 16662, calling from abroad (+372) 7943 794',
                  'EE', 'Estonia'),
                 ('Danish Poison Center (Giftlinjen): +45 8212 1212', 'DK', 'Denmark'),
                 ('+420 224 919 293, +420 224 915 402', 'CZ', 'Czech Republic'),
                 ('Phone number: 1401', 'CY', 'Cyprus'),
                 ('Telephone no +3851 2348 342. Information available 24/7 in Croatian and English.', 'HR', 'Croatia'),
                 ('The service is available 24/7 and the communication language is Bulgarian : +359 2 9154 233', 'BG',
                  'Bulgaria'),
                 ('070 245 245', 'BE', 'Belgium'),
                 ('24/24h (+43 1 406 43 43)', 'AT', 'Austria'),
                 ('22 59 13 00 (24h/24).', 'NO', 'Norway'),
                 ('NVIC: +31 (0)88 755 8000: Only for the purpose of informing medical personnel in case of acute intoxications’ or in Dutch: ‘Uitsluitend bestemd om professionele hulpverleners te informeren bij acute vergiftigingen',
                  'NL', 'the Netherlands'),
                 ('(+352) 8002 5500 Free telephone number with a 24/7 access. Experts answer all urgency questions on dangerous products in French, Dutch and English',
                  'LU', 'Luxembourg'),
                 ('+370 (85) 2362052', 'LT', 'Lithuania'),
                 ('National emergency telephone number in Latvia - to be included in section 1.4 of SDS: Valsts ugunsdzēsības un glābšanas dienests, phone number: 112. Toksikoloģijas un sepses klīnikas Saindēšanās un zāļu informācijas centrs, Hipokrāta 2, Rīga, Latvija, LV-1038, phone number +371 67042473. Service is available 24 hours',
                  'LV', 'Latvia'),
                 ('01 809 2166. 8am-10pm, 7 days a week.', 'IE', 'Ireland'),
                 ('Emergency Number : 112', 'IS', 'Iceland'),
                 ('+36-80-201-199 (0-24h, free of charge)', 'HU', 'Hungary'),
                 ('(0030) 2107793777 available for consultation 24 hours/day, to medical professionals and the public',
                  'EL', 'Greece'),
                 ('Paid service', 'DE', 'Germany'),
                 ('English : 112 – ask for Poisons Information (in Swedish this will be: "112 – begär Giftinformation").',
                  'SE', 'Sweden'),
                 ('National Emergency Telephone Number of Spanish Poison Centre: + 34 91 562 04 20. The information will be provided in Spanish (available 24h/365 days): health personnel & general public (poisoning cases)',
                  'ES', 'Spain'),
                 ('Phone number: 112', 'SI', 'Slovenia'),
                 ('Phone number: +421 2 5477 4166', 'SK', 'Slovakia'),
                 ('Phone number: +40213183606', 'RO', 'Romania'),
                 ('Portugal CIAV phone number: +351 800 250 250', 'PT', 'Portugal')),

     Countrries AS (SELECT DISTINCT p.ke, p2.*
                    FROM sdschap parentChap
                         INNER JOIN entr_sdschap_phractxtke esp
                                    ON parentChap.sdschap_id = esp.entr_src_id AND
                                       entr_tp IN ('HEADING', 'SELECTED')
                         INNER JOIN phractxtke p
                                    ON esp.entr_dst_id = p.phractxtke_id AND
                                       UPPER(TRIM(p.ke)) = 'EMERGENCY_TELEPHONE_NUMBER'
                         INNER JOIN entr_phractxtke_phractxt epp ON p.phractxtke_id = epp.entr_src_id AND
                                                                    COALESCE(epp.entr_tp, 'TRANSLATION') =
                                                                    'TRANSLATION'
                         INNER JOIN phractxt p2 ON epp.entr_dst_id = p2.phractxt_id

                    WHERE parentChap.numero LIKE '01.04_'
                      AND p2.country IS NOT NULL)
SELECT Country_Code, country
FROM Countrries
     INNER JOIN Emergency_Numbers ON UPPER(TRIM(country)) = UPPER(TRIM(Country_Code));



WITH SelectedEmergencyNumbers AS (SELECT p.*
                                  FROM phractxtke
                                       INNER JOIN entr_phractxtke_phractxt
                                                  ON phractxtke.phractxtke_id = entr_phractxtke_phractxt.entr_src_id AND
                                                     COALESCE(entr_tp, 'HAS') = 'HAS'
                                       INNER JOIN phractxt p ON entr_phractxtke_phractxt.entr_dst_id = p.phractxt_id
                                  WHERE UPPER(TRIM(ke)) = 'EMERGENCY_TELEPHONE_NUMBER'),
     UpdatedEmergencyNumbers(ENumber, Country, Country_Name) AS
         (VALUES ('33 (0)1 45 42 59 59', 'FR', 'France'),
                 ('Open 24 hours a day 0800 147 111 (the call is free of charge) 09 471 977', 'FI', 'Finlande'),
                 ('Poison information telephone number (Mürgistusteabekeskuse number) is nationally 16662, calling from abroad (+372) 7943 794', 'EE', 'Estonia'),
                 ('Danish Poison Center (Giftlinjen): +45 8212 1212', 'DK', 'Denmark'),
                 ('+420 224 919 293, +420 224 915 402', 'CZ', 'Czech Republic'),
                 ('Phone number: 1401', 'CY', 'Cyprus'),
                 ('Telephone no +3851 2348 342. Information available 24/7 in Croatian and English.', 'HR', 'Croatia'),
                 ('The service is available 24/7 and the communication language is Bulgarian : +359 2 9154 233', 'BG', 'Bulgaria'),
                 ('070 245 245', 'BE', 'Belgium'),
                 ('24/24h (+43 1 406 43 43)', 'AT', 'Austria'),
                 ('22 59 13 00 (24h/24).', 'NO', 'Norway'),
                 ('NVIC: +31 (0)88 755 8000: Only for the purpose of informing medical personnel in case of acute intoxications’ or in Dutch: ‘Uitsluitend bestemd om professionele hulpverleners te informeren bij acute vergiftigingen', 'NL', 'the Netherlands'),
                 ('(+352) 8002 5500 Free telephone number with a 24/7 access. Experts answer all urgency questions on dangerous products in French, Dutch and English', 'LU', 'Luxembourg'),
                 ('+370 (85) 2362052', 'LT', 'Lithuania'),
                 ('National emergency telephone number in Latvia - to be included in section 1.4 of SDS: Valsts ugunsdzēsības un glābšanas dienests, phone number: 112. Toksikoloģijas un sepses klīnikas Saindēšanās un zāļu informācijas centrs, Hipokrāta 2, Rīga, Latvija, LV-1038, phone number +371 67042473. Service is available 24 hours', 'LV', 'Latvia'),
                 ('01 809 2166. 8am-10pm, 7 days a week.', 'IE', 'Ireland'),
                 ('Emergency Number : 112', 'IS', 'Iceland'),
                 ('+36-80-201-199 (0-24h, free of charge)', 'HU', 'Hungary'),
                 ('(0030) 2107793777 available for consultation 24 hours/day, to medical professionals and the public', 'EL', 'Greece'),
                 ('Paid service', 'DE', 'Germany'),
                 ('English : 112 – ask for Poisons Information (in Swedish this will be: "112 – begär Giftinformation").', 'SE', 'Sweden'),
                 ('National Emergency Telephone Number of Spanish Poison Centre: + 34 91 562 04 20. The information will be provided in Spanish (available 24h/365 days): health personnel & general public (poisoning cases)', 'ES', 'Spain'),
                 ('Phone number: 112', 'SI', 'Slovenia'),
                 ('Phone number: +421 2 5477 4166', 'SK', 'Slovakia'),
                 ('Phone number: +40213183606', 'RO', 'Romania'),
                 ('Portugal CIAV phone number: +351 800 250 250', 'PT', 'Portugal'))
UPDATE phractxt
SET txt = ENumber, co = coalesce(co || ', ', '') || 'Emergency Number Updated in 21-2-2023.'
FROM UpdatedEmergencyNumbers
     INNER JOIN SelectedEmergencyNumbers USING (Country)
WHERE phractxt.phractxt_id = SelectedEmergencyNumbers.phractxt_id
  AND COALESCE(ENumber, '') != ''
;


-- 15449 =  +42 (0) 12 5477 4166


SELECT DISTINCT entr_tp--phractxtke_id, ke
FROM entr_phrac_phractxtke
--  INNER JOIN phractxtke p ON entr_phrac_phractxtke.entr_dst_id = p.phractxtke_id
--WHERE ke LIKE 'EMER%'
--ORDER BY ke;


select DISTINCT imp_ref from import.import_line;



























