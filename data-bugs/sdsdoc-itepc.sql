SELECT ns
FROM sdsdoc
WHERE split_part(ns, '/', 1) LIKE '%epc%'



WITH list_docr_fiche AS (SELECT doc_id                          AS entity_id,
                                coalesce(doc_tl, doc_file_name) AS document_name,
                                doc.tp,
                                doc.validation_dt               AS version_date,
                                doc.doc_file_name,
                                doc.country,
                                doc.cr_dt,
                                doc.st,
                                doc.md_dt                       AS online_date,
                                doc.doc_version                 AS version,
                                doc.md_dt                       AS inserted_date,
                                cast(NULL AS varchar)           AS regulatory_referential,
                                cast(NULL AS varchar)           AS regulatory_referential_ke,
                                coalesce(doc.lg, 'en')          AS lg,
                                cast(NULL AS varchar)           AS status,
                                docr_tp                         AS docr_fiche_tp,
                                cast(NULL AS varchar)           AS docr_sds_tp
                         FROM doc
                                  INNER JOIN docr_fiche ON docr_dst_id = doc_id AND docr_tp IN ('ATTACHMENT')
                         WHERE docr_src_id = :MId),
     list_docr_sds AS (SELECT doc_id                                       AS entity_id,
                              coalesce(doc_tl, doc_file_name)              AS document_name,
                              doc.tp,
                              sdsdoc.date_of_revision                      AS version_date,
                              doc.doc_file_name,
                              doc.country,
                              doc.cr_dt,
                              doc.st,
                              doc.md_dt                                    AS inserted_date,
                              doc.md_dt                                    AS online_date,
                              CASE
                                  WHEN doc.doc_version IS NULL THEN coalesce(sds_version, '0')
                                  ELSE coalesce(doc.doc_version, '0')
                                  END                                      AS version,
                              string_agg(concat(de_ke, '_#_', de_li), ',') AS regulatory_referential,
                              string_agg(de_ke, ',')                       AS regulatory_referential_ke,
                              coalesce(doc.lg, sdsdoc.lg, 'en')            AS lg,
                              sdsdoc.status,
                              ds.docr_tp                                   AS docr_sds_tp,
                              cast(NULL AS varchar)                        AS docr_fiche_tp
                       FROM doc
                                INNER JOIN docr_sds ds ON ds.docr_dst_id = doc_id AND docr_tp IN ('SDS', 'SDS_ORG')
                                INNER JOIN sdsdoc ON sdsdoc_id = ds.docr_src_id
                                INNER JOIN der_sdsdoc ON der_src_id = sdsdoc_id AND der_tp = 'VALID_FOR_REGULATORY'
                                INNER JOIN de ON de_id = der_dst_id AND de_ke = :deKe
                                INNER JOIN entr_fiche_sdsdoc ON entr_fiche_sdsdoc.entr_dst_id = sdsdoc_id AND
                                                                entr_fiche_sdsdoc.entr_tp = 'SDS'
                       WHERE entr_fiche_sdsdoc.entr_src_id = :MId
                         AND (coalesce(sdsdoc.status, '') <> 'ARCHIVED' AND coalesce(sdsdoc.status, '') <> 'E-ARCHIVED')
                       GROUP BY doc_id, coalesce(doc_tl, doc_file_name), doc.tp, sdsdoc.date_of_revision, doc.country,
                                doc.cr_dt, doc.st, doc.md_dt,
                                sds_version, doc.doc_file_name,
                                coalesce(doc.lg, sdsdoc.lg, 'en'), sdsdoc.status, ds.docr_tp, docr_fiche_tp),
     product_docs AS (SELECT entity_id,
                             doc_file_name,
                             document_name,
                             tp,
                             version_date,
                             country,
                             version,
                             regulatory_referential,
                             lg,
                             status,
                             online_date,
                             st,
                             docr_sds_tp,
                             docr_fiche_tp,
                             regulatory_referential_ke,
                             inserted_date
                      FROM list_docr_sds
                      UNION ALL
                      SELECT entity_id,
                             doc_file_name,
                             document_name,
                             tp,
                             version_date,
                             country,
                             version,
                             regulatory_referential,
                             lg,
                             status,
                             online_date,
                             st,
                             docr_sds_tp,
                             docr_fiche_tp,
                             regulatory_referential_ke,
                             inserted_date
                      FROM list_docr_fiche),
     product_docs_with_version_max AS (SELECT docr_sds_tp,
                                              max(version)      AS max_version,
                                              max(online_date)  AS max_online_date,
                                              max(version_date) AS max_version_date
                                       FROM product_docs
                                       GROUP BY docr_sds_tp)
SELECT entity_id,
       doc_file_name,
       document_name,
       tp,
       version_date,
       country,
       version,
       regulatory_referential,
       regulatory_referential_ke,
       lg,
       status,
       online_date,
       st,
       product_docs.docr_sds_tp,
       docr_fiche_tp,
       inserted_date
FROM product_docs
         INNER JOIN product_docs_with_version_max
                    ON product_docs_with_version_max.docr_sds_tp = product_docs.docr_sds_tp
                        AND product_docs_with_version_max.max_version = product_docs.version;


SELECT *
FROM fiche
WHERE m_id = :MId

SELECT *
FROM de
WHERE de_id = 64159;

SELECT *
FROM de
WHERE de_id = 255;

SELECT tp, *
FROM sdsdoc
WHERE ns ILIKE '%epc%';

UPDATE sdsdoc
SET tp = 'SDS'
WHERE ns ILIKE '%epc%'


SELECT der_dst_id
FROM sdsdoc
         INNER JOIN der_sdsdoc ds ON sdsdoc.sdsdoc_id = ds.der_src_id
WHERE ns ILIKE '%epc%'

SELECT sdsdoc_id, ess.entr_dst_id, lg
FROM sdsdoc
         LEFT JOIN entr_sdsdoc_sdschap ess ON sdsdoc.sdsdoc_id = ess.entr_src_id
WHERE ns ILIKE '%epc%'

SELECT DISTINCT entr_tp
FROM entr_sdsdoc_sdschap;

SELECT sdschap_id
FROM sdschap;

INSERT INTO entr_sdsdoc_sdschap(entr_id, entr_src_id, entr_dst_id, entr_tp, entr_co)
VALUES (nextval('seq_entr_sdsdoc_sdschap_id'), 100018868, 1, 'CHILD_CHAPTER', 'test-fix')


SELECT st, *
FROM doc
WHERE doc_id = 773720;

UPDATE doc
SET st = 'VALID'
WHERE doc_id = 773720;

SELECT
    * from substances;
