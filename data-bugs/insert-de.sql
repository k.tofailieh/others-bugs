/* ADD New Product Documents On Kyoto */
WITH product_documents (de_tp, de_li, de_ke, de_st)
         AS (VALUES ('REGULATORY_REFERENTIAL', 'HCS/HazCom 2013', 'HCS2013', 'VALID'),
                    ('REGULATORY_REFERENTIAL', 'FORMULA', 'FORMULA', 'VALID'),
                    ('COSMETIC_PRODUCT_DOC_TYPE', 'BATCH_NUMBERING_PROCEDURE', 'BATCH_NUMBERING_PROCEDURE', 'VALID'),
                    ('COSMETIC_PRODUCT_DOC_TYPE', 'FSE', 'FSE', 'VALID'),
                    ('COSMETIC_PRODUCT_DOC_TYPE', 'IFRA_CERTIFICATE', 'IFRA_CERTIFICATE', 'VALID'))
INSERT
INTO de(de_id, de_tp, de_li, de_ke, de_st)
SELECT coalesce(de_id, nextval('seq_de_id')),
       product_documents.de_tp,
       product_documents.de_li,
       product_documents.de_ke,
       product_documents.de_st
FROM product_documents
         LEFT JOIN de ON trim(upper(product_documents.de_ke)) = trim(upper(de.de_ke))
    AND trim(upper(product_documents.de_li)) = trim(upper(de.de_li))
    AND trim(upper(product_documents.de_tp)) = trim(upper(de.de_tp))
-- if they are already exists set de_st = 'VALID' if it is null.
ON CONFLICT (de_id) DO UPDATE SET de_st = 'VALID'
WHERE de.de_st IS NULL
RETURNING de_id;