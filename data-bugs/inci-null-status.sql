
/*
To get a correct result please put the columns in import_line like this:
    inci_id: internal_id,
    anx_st: value1,
    cas: value2,
    inciname: value3,
    tp: value5,
    comments: value6,
    Extra: value7.
*/

/* update imp_err = True for duplicated INCIs*/
WITH inci_with_count AS (SELECT inci_id, inciname, count(inci_id) OVER (PARTITION BY inciname) inci_cnt
                         FROM anx.inci)
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || 'Duplicated INCI.'
FROM inci_with_count
WHERE inci_id = internal_id
  AND inci_cnt > 1
  AND imp_ref = :imp_ref;

/* For the records that colored by "Pink" in the file:
   - Check if the inci-name exists as incitranslation for CI****, if not add it as incitranslation and link it with CI****. */
WITH colorent_incis AS (SELECT imp_id,
                               internal_id          inci_id,
                               value1,
                               value2,
                               value3            AS transaltion,
                               trim(replace(
                                       translate(value6, 'abcdefghijklmnopqrstuvwxyzABDEFGHJKLMNOPQRSTUVWXYZ,', '#'),
                                       '#', '')) AS colorent
                        FROM import.import_line
                        WHERE lower(value6) LIKE 'to delete%'
                          AND coalesce(value7, '') != ''
                          AND imp_ref = :imp_ref),
     exists_translation AS (SELECT DISTINCT anx.inci.inci_id        AS colorent_inci_id,
                                            anx.inci.inciname,
                                            colorent                AS colorent_inci_name,
                                            incitranslation.tuv_seg AS db_translation,
                                            colorent_incis.transaltion,
                                            colorent_incis.imp_id
                            FROM colorent_incis
                                     INNER JOIN anx.inci ON trim(colorent) = trim(inciname)
                                     LEFT JOIN anx.entr_inci_incitranslation
                                               ON inci.inci_id = entr_inci_incitranslation.entr_src_id
                                     LEFT JOIN anx.incitranslation ON entr_inci_incitranslation.entr_dst_id =
                                                                      incitranslation.incitranslation_id AND
                                                                      trim(tuv_seg) = trim(transaltion)
                            WHERE tuv_seg IS NULL)
/* update import_line to flag what to insert. */
UPDATE import.import_line
SET value10   = 'ADD_TRANSLATION',
    value_id1 = colorent_inci_id,
    value_id2 = - nextval('anx.seq_incitranslation_id')
FROM exists_translation
WHERE exists_translation.imp_id = import_line.imp_id
  AND coalesce(imp_err, FALSE) = FALSE;

/* For the records that colored by "Yellow" in the file:
   -add the inciname as incitranslation for the mentioned CI****. */
WITH extract_new_inciname AS (SELECT imp_id,
                                     internal_id       AS inci_id,
                                     value3            AS inciname,
                                     trim(replace(
                                             translate(value7, 'abcdefghijklmnopqrstuvwxyzABDEFGHJKLMNOPQRSTUVWXYZ,?',
                                                       '#'),
                                             '#', '')) AS new_inciname
                              FROM import.import_line
                              WHERE lower(value6) LIKE '%correponding does not exist%'
                                AND imp_ref = :imp_ref)
/* update import line to flag what to insert. */
UPDATE import.import_line
SET value9   = new_inciname,
    value10 = 'ADD_TRANSLATION',
    value_id1 = internal_id,
    value_id2 = - nextval('anx.seq_incitranslation_id')
FROM extract_new_inciname
WHERE import_line.imp_id = extract_new_inciname.imp_id
AND coalesce(imp_err, FALSE) = FALSE;

/* insert the incitranslations. */
INSERT INTO anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, lg, country, co)
SELECT abs(value_id2), value3, upper(value3), 'en', 'US', 'inci-null-status-update'
FROM import.import_line
WHERE imp_ref = :imp_ref
  AND coalesce(imp_err, FALSE) = FALSE
  AND value_id2 < 0
  AND value10 = 'ADD_TRANSLATION'
  and imp_ref = :imp_ref;

/* link each inci with it's incitranslation. */
INSERT INTO anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp)
    (SELECT nextval('anx.seq_entr_inci_incitranslation_id'), value_id1, abs(value_id2), 'TRANSLATION' AS tp
     FROM import.import_line
     WHERE imp_ref = :imp_ref
       AND value_id2 < 0
       AND value10 = 'ADD_TRANSLATION');


/* update the status of the records that flagged with "to keep" and have no warnings. */
UPDATE anx.inci
SET anx_st = 'VALID',
    anx_co = coalesce(anx_co, '') || '|update-status-valid'
FROM import.import_line
WHERE internal_id = inci_id
  AND coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;


/* delete the records that flagged with "to delete" and have no warnings */
DELETE
FROM anx.inci
    USING import.import_line
WHERE internal_id = inci_id
  AND trim(upper(value6)) LIKE '%DELETE%'
  AND imp_ref = :imp_ref;

/* extract duplicated incis (not updated)*/
select internal_id as inci_id,
       value3 as inciname,
       imp_err_msg as error_message
from import.import_line
where imp_err = TRUE
and imp_ref = :imp_ref;
