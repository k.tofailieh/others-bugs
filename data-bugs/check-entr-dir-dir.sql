
-- 91 records have this situation (entr_tp = null and src_id = dst_id).
SELECT count(entr_id) -- , to_char(edd.tpsfrom, 'yyyy')
FROM entr_dir_dir edd
WHERE edd.entr_tp IS NULL
  AND entr_src_id = entr_dst_id;
--GROUP BY  to_char(edd.tpsfrom, 'yyyy');


-- Useful to determine that the right entr_tp is one of the values: (MEMBER 3 records, SUPPLIER 16 records)
SELECT entr_tp , count(DISTINCT entr_id) --, to_char(edd.tpsfrom, 'yyyy')
FROM entr_dir_dir edd
WHERE edd.entr_tp IS NOT NULL
  AND entr_src_id = entr_dst_id
GROUP BY entr_tp  --, to_char(edd.tpsfrom, 'yyyy')
;

-- counts of entr_tp: 'MEMBER'= 15275,'SUPPLIER'= 46637.
-- the normal case, where entr_tp is not null:
SELECT entr_tp, count(DISTINCT entr_id)
FROM entr_dir_dir edd
WHERE edd.entr_tp IN ('MEMBER', 'SUPPLIER')
GROUP BY entr_tp
;

-- 5 records where (src != dst)
-- all records have no information and have no relations can help us to determine the right entr_tp.
-- maybe we should delete these 5 records.
SELECT d1.*, d2.*
FROM dir d1
         INNER JOIN entr_dir_dir edd ON d1.dir_id = edd.entr_src_id
         INNER JOIN dir d2 ON d2.dir_id = edd.entr_dst_id
WHERE entr_tp IS NULL
  AND edd.entr_dst_id != edd.entr_src_id
;
