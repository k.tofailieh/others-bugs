/*
To get a correct result please put the columns in import_line like this:
    inci_id: internal_id,
    anx_st: value1,
    cas: value2,
    inciname: value3,
    tp: value5,
    comments: value6,
    Extra: value7.
*/

/* Begin:
    -Flagging the records in import_line. */
------------------------------------------------------------------------------------------------------------------------
/* Update imp_err = True for duplicated INCIs. */
WITH inci_with_count AS (SELECT inci_id, inciname, count(inci_id) OVER (PARTITION BY inciname) inci_cnt
                         FROM anx.inci)
UPDATE import.import_line
SET imp_err     = TRUE,
    imp_err_msg = coalesce(imp_err_msg, '') || 'Duplicated INCI.'
FROM inci_with_count
WHERE inci_id = internal_id
  AND inci_cnt > 1
  AND imp_ref = :imp_ref;

-- comment: if you say that inci_id= internal_id for sure you will not find duplicates because the inci_id is unique. you should find the duplicates based on the inciname.
/* yes here in the first subquery I got the count of the inci's based on their inciname(I used partition by), and I selected the inci_id with it.
    so in the result set we have the count for each inci(based on the inciname) with it's inci_id. */

/* For the records that colored by "Pink" in the file:
   - Check if the inci-name exists as incitranslation for CI****, if not add it as incitranslation and link it with CI****. */
WITH colorent_incis AS (SELECT imp_id,
                               internal_id                               inci_id,
                               value1,
                               value2,
                               value3                                 AS transaltion,
                               unnest(regexp_match(value6, 'CI \d+')) AS colorent
                        FROM import.import_line
                        WHERE lower(value6) LIKE 'to delete%'
                          AND imp_ref = :imp_ref),
     exists_translation AS (SELECT DISTINCT anx.inci.inci_id        AS colorent_inci_id,
                                            anx.inci.inciname,
                                            colorent                AS colorent_inci_name,
                                            incitranslation.tuv_seg AS db_translation,
                                            colorent_incis.transaltion,
                                            colorent_incis.imp_id
                            FROM colorent_incis
                                     INNER JOIN anx.inci ON trim(upper(colorent)) = trim(upper(inciname))
                                     LEFT JOIN anx.entr_inci_incitranslation
                                               ON inci.inci_id = entr_inci_incitranslation.entr_src_id
                                     LEFT JOIN anx.incitranslation ON entr_inci_incitranslation.entr_dst_id =
                                                                      incitranslation.incitranslation_id AND
                                                                      trim(upper(tuv_seg)) = trim(upper(transaltion))
                                 --comment: you can use UPPER in both sides to make sure it's not case sensitive
                            WHERE tuv_seg IS NULL)
/* update import_line to flag what to insert. */
UPDATE import.import_line
SET value9    = colorent_inci_name,
    value10   = 'ADD_TRANSLATION',
    value_id1 = colorent_inci_id,
    value_id2 = - nextval('anx.seq_incitranslation_id')
FROM exists_translation
WHERE exists_translation.imp_id = import_line.imp_id
  AND coalesce(imp_err, FALSE) = FALSE;

/* For the records that colored by "Yellow" in the file:
   -add the inciname as incitranslation for the mentioned CI****. */
WITH extract_new_inciname AS (SELECT imp_id,
                                     internal_id                            AS inci_id,
                                     value3                                 AS inciname,
                                     unnest(regexp_match(value6, 'CI \d+')) AS new_inciname
                                     --comment: shouldn't be value6? (done)
                              FROM import.import_line
                              WHERE lower(value6) LIKE '%correponding does not exist%'
                                AND imp_ref = :imp_ref)
/* update import line to flag what to insert. */
UPDATE import.import_line
SET value9    = new_inciname,
    value10   = 'ADD_TRANSLATION_UPDATE_INCINAME',
    value_id1 = internal_id,
    value_id2 = - nextval('anx.seq_incitranslation_id')
FROM extract_new_inciname
WHERE import_line.imp_id = extract_new_inciname.imp_id
  AND coalesce(imp_err, FALSE) = FALSE;

/* Insert the new translations and link them. */
------------------------------------------------------------------------------------------------------------------------
/* Insert the incitranslations. */
INSERT INTO anx.incitranslation(incitranslation_id, tuv_seg, tuv_charonly, lg, country, co)
SELECT abs(value_id2),
       trim(value3),
       upper(trim(value3)),
       'en',
       'US',
       'Colorant Translation for: ' || coalesce(value9, '')
--comment: in the CO you can say Colorant transltion CIXXX; use trim for inserting the String values
FROM import.import_line
WHERE coalesce(imp_err, FALSE) = FALSE
  AND value_id2 < 0
  AND value10 IN ('ADD_TRANSLATION', 'ADD_TRANSLATION_UPDATE_INCINAME')
  AND imp_ref = :imp_ref;

/* Link each inci with it's incitranslation. */
INSERT INTO anx.entr_inci_incitranslation(entr_id, entr_src_id, entr_dst_id, entr_tp)
    (SELECT nextval('anx.seq_entr_inci_incitranslation_id'), value_id1, abs(value_id2), 'TRANSLATION' AS tp
     FROM import.import_line
     WHERE imp_ref = :imp_ref
       AND value_id2 < 0
       AND value10 IN ('ADD_TRANSLATION', 'ADD_TRANSLATION_UPDATE_INCINAME'));

/* Modify records in DB. */
------------------------------------------------------------------------------------------------------------------------
/* Delete the records that flagged with "to delete". */
--comment: it's better to delete the INCIs before updating their status (done)
DELETE
FROM anx.inci
    USING
        IMPORT.import_line
WHERE internal_id = inci_id
  AND TRIM(upper(value6)) LIKE '%DELETE%'
  AND imp_ref = :imp_ref;

/* Update the status of the records that flagged with "to keep" and have no warnings. */
UPDATE anx.inci
SET anx_st = 'VALID',
    anx_co = coalesce(anx_co, '') || '| update-status-valid'
FROM import.import_line
WHERE internal_id = inci_id
  AND coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref;

--Comment: were do you update the inciname of the Colorants? to replace it with CIXXX format(done)?
--			and also you need to update the CAS number for the ones that have CAS number in the file(done).

/* Update inci-name for records in 'Yellow' color in the file. */
UPDATE anx.inci
SET inciname = trim(value9)
FROM import.import_line
WHERE inci_id = internal_id
  AND coalesce(imp_err, FALSE) = FALSE
  AND value10 = 'ADD_TRANSLATION_UPDATE_INCINAME'
  AND imp_ref = :imp_ref;

/* Update cas for the records that have cas in the file and it different from the existing cas. */
UPDATE anx.inci
SET cas = coalesce(nullif(trim(value2), ''), cas)
FROM import.import_line
WHERE coalesce(imp_err, FALSE) = FALSE
  AND imp_ref = :imp_ref
  AND trim(cas) != trim(value2);

/* End:
    -Reporting */
------------------------------------------------------------------------------------------------------------------------
/* Extract duplicated INCIs (not updated)*/
SELECT internal_id AS inci_id,
       value3      AS inciname,
       imp_err_msg AS error_message
FROM import.import_line
WHERE imp_err = TRUE
  AND imp_ref = :imp_ref;
------------------------------------------------------------------------------------------------------------------------