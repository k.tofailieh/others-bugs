WITH product AS (SELECT m_id, m_ref, marguage,
                        clps, clpsh
                 FROM (SELECT m_id, m_ref, marguage,
                           /* CLP */
                              lCLP.clps,
                           /* CLP-Hnnn */
                              lCLP.clpsh
                       FROM fiche
/* Site */
                            LEFT OUTER JOIN entr_presence_fiche AS epf
                                            ON epf.entr_dst_id = m_id AND epf.entr_tp = 'MATERIAL'
                            LEFT OUTER JOIN entr_site_presence AS esp
                                            ON esp.entr_dst_id = epf.entr_src_id AND esp.entr_tp = 'MATERIAL'
                            LEFT OUTER JOIN site ON site_id = esp.entr_src_id
/* CLP */
                            LEFT OUTER JOIN
                                            (SELECT efp.entr_src_id, STRING_AGG(DISTINCT HClassCat.plcsys_ke, ', '
                                                                                ORDER BY HClassCat.plcsys_ke) AS clps,
                                                    STRING_AGG(DISTINCT plcsys.plcsys_ke, ', ' ORDER BY plcsys.plcsys_ke) AS clpsh
                                             FROM entr_fiche_plc efp
                                                  INNER JOIN plc ON efp.entr_dst_id = plc.plc_id
                                                  INNER JOIN entr_plc_plcsys epps
                                                             ON plc.plc_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
                                                  INNER JOIN plcsys HClassCat ON HClassCat.plcsys_id = epps.entr_dst_id
                                                  INNER JOIN entr_plcsys_plcsys epsps
                                                             ON epsps.entr_src_id = epps.entr_dst_id AND epsps.entr_tp =
                                                                                                         CONCAT('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
                                                  INNER JOIN plcsys
                                                             ON plcsys.plcsys_id = epsps.entr_dst_id AND plcsys.name = 'GHS_H_STMT'
                                             WHERE efp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                                             GROUP BY efp.entr_src_id) lCLP ON lCLP.entr_src_id = m_id
                       WHERE COALESCE(fiche.active, 0) <> 0
                         AND (fiche.ns LIKE :user_organisation || '/%' OR fiche.ns = :user_organisation)) t
                 GROUP BY m_id, m_ref, marguage, clps, clpsh),
     vlep AS (SELECT m_id AS product_id, sub_id, MIN(ers.entr_float2) AS min_vle, COUNT(reg.reg_id) AS nb_country,
                     STRING_AGG(DISTINCT de_ke, ', ' ORDER BY de_ke) AS countries
              FROM fiche
                   INNER JOIN entr_fiche_nomclit efn ON efn.entr_src_id = m_id AND efn.entr_tp = 'REAL_COMPO_SUBSTANCE'
                   INNER JOIN entr_nomclit_substances ens
                              ON ens.entr_src_id = efn.entr_dst_id AND ens.entr_tp = 'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
                   INNER JOIN substances s ON s.sub_id = ens.entr_dst_id
                   INNER JOIN entr_reg_substances ers ON ers.entr_src_id = s.sub_id
                   INNER JOIN reg ON reg.reg_id = ers.entr_dst_id AND reg.reg_ke LIKE 'OCC_EXPOSURE_LIMITS%'
                   INNER JOIN der_reg ON der_src_id = reg_id AND der_tp = 'REGULATION_COUNTRY'
                   INNER JOIN de ON de_id = der_dst_id
              WHERE COALESCE(fiche.active, 0) <> 0
                AND (fiche.ns LIKE :user_organisation || '/%' OR fiche.ns = :user_organisation)
              GROUP BY m_id, sub_id)
SELECT m_id, m_ref, marguage,
    /* CLP */
       product.clps,
    /* CLP - Hnnn */
       product.clpsh,
    /* composition */
       composition.conc_real_lbound_inc,
       MIN(composition.conc_real_lbound) AS concentration_min,
       composition.conc_real_ubound_inc,
       MAX(composition.conc_real_ubound) AS concentration_max,
       substances.cas, substances.ec, substances.idx, substances.naml,
       (
           EXISTS(SELECT 1
                  FROM entr_nomclit_plc
                       INNER JOIN entr_plc_plcsys ON entr_plc_plcsys.entr_src_id = entr_nomclit_plc.entr_dst_id AND
                                                     entr_plc_plcsys.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK')
                       INNER JOIN plcsys ON plcsys.plcsys_id = entr_plc_plcsys.entr_dst_id
                  WHERE entr_nomclit_plc.entr_src_id = nomclit_id
                    AND entr_nomclit_plc.entr_tp = CONCAT('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
                    AND plcsys.plcsys_ke SIMILAR TO '(Muta. 1|Carc. 1|Repr. 1|R45|R46|R49|R60|R61)%'
               )
           ) AS cmr1,
       (
           EXISTS(SELECT 1
                  FROM entr_nomclit_plc
                       INNER JOIN entr_plc_plcsys ON entr_plc_plcsys.entr_src_id = entr_nomclit_plc.entr_dst_id AND
                                                     entr_plc_plcsys.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK')
                       INNER JOIN plcsys ON plcsys.plcsys_id = entr_plc_plcsys.entr_dst_id
                  WHERE entr_nomclit_plc.entr_src_id = nomclit_id
                    AND entr_nomclit_plc.entr_tp = CONCAT('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
                    AND plcsys.plcsys_ke SIMILAR TO '(Muta. 2|Carc. 2|Repr. 2|R40|R62|R63|R68)%'
               )
           ) AS cmr2,
       COALESCE(vlep.min_vle, 0) AS min_vle,
       COALESCE(vlep.nb_country, 0) AS nb_country,
       COALESCE(vlep.countries, '') AS countries,
       sCLP.name AS substance_classification_name,
       sCLP.clps AS substance_classification
FROM product
/* Composition */
     LEFT OUTER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE
                     ON ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_src_id = product.m_id AND
                        ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_tp = 'REAL_COMPO_SUBSTANCE'
     LEFT OUTER JOIN nomclit composition ON composition.nomclit_id = ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_dst_id
     LEFT OUTER JOIN ENTR_nomclit_substances ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS
                     ON ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_src_id =
                        composition.nomclit_id AND ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_tp =
                                                   'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
     LEFT OUTER JOIN substances substances
                     ON substances.sub_id = ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_dst_id
     LEFT OUTER JOIN entr_subset_substances svhc_sub ON svhc_sub.entr_dst_id = sub_id AND svhc_sub.entr_tp = 'IS_IN' AND
                                                        svhc_sub.entr_src_id =
                                                        (SELECT subset_id FROM subset WHERE key = 'All SVHC')
     LEFT OUTER JOIN vlep ON vlep.product_id = product.m_id AND vlep.sub_id = substances.sub_id
     LEFT OUTER JOIN entr_reg_substances registred_sub
                     ON registred_sub.entr_dst_id = substances.sub_id AND registred_sub.entr_tp = 'SUBSTANCES' AND
                        registred_sub.entr_src_id = 100
     LEFT OUTER JOIN entr_reg_substances exempted_sub
                     ON exempted_sub.entr_dst_id = substances.sub_id AND exempted_sub.entr_tp = 'SUBSTANCES' AND
                        exempted_sub.entr_src_id = 27
/* component CLP */
     LEFT OUTER JOIN
                     (SELECT enp.entr_src_id AS sCLP_nomclit_id, plc.name,
                             STRING_AGG(DISTINCT plcsys.plcsys_ke, ', ' ORDER BY plcsys.plcsys_ke) AS clps
                      FROM entr_nomclit_plc enp
                           INNER JOIN plc ON enp.entr_dst_id = plc.plc_id
                           INNER JOIN entr_plc_plcsys epps
                                      ON plc.plc_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
                           INNER JOIN plcsys ON plcsys.plcsys_id = epps.entr_dst_id
                      WHERE enp.entr_tp = CONCAT('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
                      GROUP BY enp.entr_src_id, plc.name) sCLP ON sCLP_nomclit_id = composition.nomclit_id
GROUP BY m_id, m_ref, marguage,
         product.clps, product.clpsh,
         composition.conc_real_lbound_inc,
         composition.conc_real_lbound,
         composition.conc_real_ubound_inc,
         composition.conc_real_ubound,
         substances.cas, substances.ec, substances.idx, substances.naml, cmr1, cmr2,
         min_vle, nb_country, countries, substance_classification_name, substance_classification
ORDER BY m_ref, marguage, substances.naml, substances.cas, substances.ec, composition.conc_real_lbound_inc,
         concentration_min, composition.conc_real_ubound_inc, concentration_max DESC
