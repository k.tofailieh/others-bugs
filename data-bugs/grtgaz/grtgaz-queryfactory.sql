SELECT m_id,
       m_ref,
       UFI.der_char1 AS UFI,
       marguage AS code_M3,
       entr_creator.entr_dt1 AS creation_date,
       entr_modificator.entr_dt1 AS modification_date,
       fiche_nt AS utilisation,
       displayname AS supplier_name,
       entr_supplier.entr_char1 AS supplier_code,
       dir_supplier.postalcode,
       dir_supplier.postaladdress,
       dir_supplier.city,
       dir_supplier.country,
       manufacturer,
       manufacturer_ref,
       STRING_AGG(DISTINCT site_utilisation, ', ' ORDER BY site_utilisation) AS site_utilisation,
    /*Classification & Label elements CLP & DPD*/
       hnnn,
       pnnn,
       rnnn,
       snnn,
       REPLACE(picto, 'GHS', 'SGH') AS picto,
    /*SDS data attachement*/
       ventr_fiche_sdsdoc_attachment.name AS sds_name,
       ventr_fiche_sdsdoc_attachment.lg AS sds_lg,
       ventr_fiche_sdsdoc_attachment.sds_version,
       TO_CHAR(ventr_fiche_sdsdoc_attachment.date_of_revision, 'DD/MM/YYYY') AS date_of_revision,
       TO_CHAR(ventr_fiche_sdsdoc_attachment.sds_valid1_dt, 'DD/MM/YYYY') AS date_of_reception,
    /*Simplified SDS data*/
       ventr_fiche_sdsdoc.name AS simplified_sds_name,
       ventr_fiche_sdsdoc.lg AS simplified_sds_lg,
       ventr_fiche_sdsdoc.sds_version AS simplified_sds_version,
       TO_CHAR(ventr_fiche_sdsdoc.date_of_revision, 'DD/MM/YYYY') AS simplified_sds_date_of_revision,
    /*Extends FDS data*/
       ventr_fiche_sdse.doc_file_name AS sdse_name,
       ventr_fiche_sdse.lg AS sdse_lg,
       TO_CHAR(ventr_fiche_sdse.doc_cr_dt, 'DD/MM/YYYY') AS sdse_date_of_reception,
       ventr_fiche_sdse.doc_version AS sdse_version,
       TO_CHAR(ventr_fiche_sdse.validation_dt, 'DD/MM/YYYY') AS sdse_date_of_revision,
       warningtb.danger,
       ventr_fiche_PHYSSTATE.val AS Physical_State,
       ventr_fiche_flashpoint.value_operator AS point_eclair_operator,
       ventr_fiche_flashpoint.val AS point_eclair_value,
       ventr_fiche_flashpoint.value_unit AS point_eclair_value_unit,
       ventr_fiche_boilingpoint.value_operator AS point_ebullition_operator,
       ventr_fiche_boilingpoint.val AS point_ebullition_value,
       ventr_fiche_boilingpoint.value_unit AS point_ebullition_value_unit,
       ventr_fiche_ph.val AS ph,
       ventr_fiche_cinematic_viscosity.val AS cinematic_viscosity,
       ventr_fiche_cov.val AS cov,
       ventr_fiche_vapour_pressure.value_operator AS vapour_pressure_operator,
       ventr_fiche_vapour_pressure.val AS vapour_pressure_value,
       ventr_fiche_vapour_pressure.value_unit AS vapour_pressure_unit
FROM fiche
         /*PRESENCE Site utilisation*/
     LEFT OUTER JOIN (SELECT STRING_AGG(site_li, ', ' ORDER BY site_li) AS site_utilisation,
                             epf.entr_dst_id,
                             epf.entr_tp AS linked_site_tp,
                             presence_tp
                      FROM entr_presence_fiche epf
                           INNER JOIN presence ON epf.entr_src_id = presence_id
                           INNER JOIN entr_site_presence esp ON esp.entr_dst_id = presence_id
                           INNER JOIN site ON esp.entr_src_id = site_id
                      GROUP BY site_li, epf.entr_dst_id, presence_tp, epf.entr_tp) ventr_fiche_presence
                     ON ventr_fiche_presence.entr_dst_id = m_id
                         AND ventr_fiche_presence.linked_site_tp = 'MATERIAL'
                         /*MODIFICATOR*/
     LEFT OUTER JOIN (SELECT entr_src_id,
                             entr_tp,
                             entr_dt1,
                             RANK() OVER (PARTITION BY entr_src_id ORDER BY entr_dt1 DESC) AS rank_modificator
                      FROM entr_fiche_dir) entr_modificator ON entr_modificator.entr_src_id = m_id
    AND entr_modificator.entr_tp = 'MODIFICATOR'
    AND (entr_modificator.rank_modificator = 1 OR entr_modificator.rank_modificator IS NULL)
                         /*CREATOR*/
     LEFT OUTER JOIN entr_fiche_dir entr_creator ON entr_creator.entr_src_id = m_id AND entr_creator.entr_tp = 'CREATOR'
                         /*UFI*/
     LEFT OUTER JOIN der_fiche UFI ON UFI.der_src_id = m_id AND UFI.der_tp = 'CURRENT_OFFICIAL_UFI'
                         /*SUPPLIER*/
     LEFT OUTER JOIN entr_fiche_dir entr_supplier
                     ON entr_supplier.entr_src_id = m_id AND entr_supplier.entr_tp = 'SUPPLIER'
     LEFT OUTER JOIN dir dir_supplier ON entr_supplier.entr_dst_id = dir_id
                         /*CLP_S  H_STMT*/
     LEFT OUTER JOIN (SELECT STRING_AGG(DISTINCT hnnn.plcsys_ke, ', ' ORDER BY hnnn.plcsys_ke) AS hnnn,
                             esp_h.entr_src_id,
                             esp_h.entr_tp
                      FROM entr_fiche_plc AS esp_h
                           LEFT OUTER JOIN plc plc_h ON plc_h.plc_id = esp_h.entr_dst_id
                           LEFT OUTER JOIN entr_plc_plcsys epp1_h
                                           ON epp1_h.entr_src_id = plc_h.plc_id AND epp1_h.entr_tp = 'HAZARD_CLASS_CAT'
                           LEFT OUTER JOIN plcsys p1_h ON epp1_h.entr_dst_id = p1_h.plcsys_id AND p1_h.tp = 'CATEGORIZE'
                           LEFT OUTER JOIN entr_plcsys_plcsys epp2_h ON epp2_h.entr_src_id = p1_h.plcsys_id AND
                                                                        epp2_h.entr_tp =
                                                                        CONCAT('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
                           LEFT OUTER JOIN plcsys hnnn ON epp2_h.entr_dst_id = hnnn.plcsys_id AND hnnn.tp = 'H_STMT'
                      WHERE esp_h.entr_tp LIKE CONCAT('%', :REGULATORY_REFERENTIAL)
                      GROUP BY esp_h.entr_src_id, esp_h.entr_tp) ventr_fiche_plcsys_h
                     ON ventr_fiche_plcsys_h.entr_src_id = m_id AND
                        ventr_fiche_plcsys_h.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                         /*CLP_S  P_STMT*/
     LEFT OUTER JOIN (SELECT STRING_AGG(DISTINCT pnnn.plcsys_ke, ', ' ORDER BY pnnn.plcsys_ke) AS pnnn,
                             esp_p.entr_src_id,
                             esp_p.entr_tp
                      FROM entr_fiche_plc esp_p
                           LEFT OUTER JOIN plc plc_p ON plc_p.plc_id = esp_p.entr_dst_id
                           LEFT OUTER JOIN entr_plc_plcsys epp1_p
                                           ON epp1_p.entr_src_id = plc_p.plc_id AND epp1_p.entr_tp = 'HAZARD_CLASS_CAT'
                           LEFT OUTER JOIN plcsys p1_p ON epp1_p.entr_dst_id = p1_p.plcsys_id AND p1_p.tp = 'CATEGORIZE'
                           LEFT OUTER JOIN entr_plcsys_plcsys epp2_p ON epp2_p.entr_src_id = p1_p.plcsys_id AND
                                                                        epp2_p.entr_tp =
                                                                        CONCAT('PSTMT_', :REGULATORY_REFERENTIAL)
                           LEFT OUTER JOIN plcsys pnnn ON epp2_p.entr_dst_id = pnnn.plcsys_id AND pnnn.name = 'P_STMT'
                      WHERE esp_p.entr_tp LIKE CONCAT('%', :REGULATORY_REFERENTIAL)
                      GROUP BY esp_p.entr_src_id, esp_p.entr_tp) ventr_fiche_plcsys_p
                     ON ventr_fiche_plcsys_p.entr_src_id = m_id AND
                        ventr_fiche_plcsys_p.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                         /*PICTO*/
     LEFT OUTER JOIN (SELECT STRING_AGG(DISTINCT pic.plcsys_ke, ', ' ORDER BY pic.plcsys_ke) AS picto,
                             esp_pic.entr_src_id,
                             esp_pic.entr_tp
                      FROM entr_fiche_plc esp_pic
                           LEFT OUTER JOIN plc plc_pic ON plc_pic.plc_id = esp_pic.entr_dst_id
                           LEFT OUTER JOIN entr_plc_plcsys epp1_pic ON epp1_pic.entr_src_id = plc_pic.plc_id AND
                                                                       epp1_pic.entr_tp = 'HAZARD_CLASS_CAT'
                           LEFT OUTER JOIN plcsys p1_pic ON epp1_pic.entr_dst_id = p1_pic.plcsys_id
                           LEFT OUTER JOIN entr_plcsys_plcsys epp2_pic ON epp2_pic.entr_src_id = p1_pic.plcsys_id AND
                                                                          epp2_pic.entr_tp =
                                                                          CONCAT('GHS_PICTO_', :REGULATORY_REFERENTIAL)
                           LEFT OUTER JOIN plcsys pic ON epp2_pic.entr_dst_id = pic.plcsys_id AND pic.name = 'GHS_PICTO'
                      WHERE esp_pic.entr_tp LIKE CONCAT('%', :REGULATORY_REFERENTIAL)
                      GROUP BY esp_pic.entr_src_id, esp_pic.entr_tp) ventr_fiche_plcsys_pic
                     ON ventr_fiche_plcsys_pic.entr_src_id = m_id AND
                        ventr_fiche_plcsys_pic.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                         /*WARNING*/
     LEFT OUTER JOIN (SELECT efp.entr_src_id, efp.entr_tp, MIN(pls1.plcsys_ke) AS danger
                      FROM entr_fiche_plc efp
                           INNER JOIN entr_plc_plcsys epps
                                      ON efp.entr_dst_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
                           INNER JOIN entr_plcsys_plcsys epsps ON epsps.entr_src_id = epps.entr_dst_id AND
                                                                  epsps.entr_tp IN
                                                                  (CONCAT('SIGNALWORD_', :REGULATORY_REFERENTIAL))
                           INNER JOIN plcsys pls1 ON pls1.plcsys_id = epsps.entr_dst_id
                      GROUP BY efp.entr_src_id, efp.entr_tp) warningtb
                     ON warningtb.entr_src_id = m_id AND warningtb.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                         /*DPD_S  R_STMT*/
     LEFT OUTER JOIN (SELECT STRING_AGG(DISTINCT rnnn.plcsys_ke, ', ' ORDER BY rnnn.plcsys_ke) AS rnnn,
                             esp_r.entr_src_id,
                             esp_r.entr_tp
                      FROM entr_fiche_plc esp_r
                           LEFT OUTER JOIN plc plc_r ON plc_r.plc_id = esp_r.entr_dst_id
                           LEFT OUTER JOIN entr_plc_plcsys epp1_r
                                           ON epp1_r.entr_src_id = plc_r.plc_id AND epp1_r.entr_tp = 'RISK'
                           LEFT OUTER JOIN plcsys rnnn ON epp1_r.entr_dst_id = rnnn.plcsys_id AND rnnn.tp = 'RISK'
                      GROUP BY esp_r.entr_src_id, esp_r.entr_tp) ventr_fiche_plcsys_r
                     ON ventr_fiche_plcsys_r.entr_src_id = m_id AND ventr_fiche_plcsys_r.entr_tp = 'DPD_S'
                         /*DPD_S  S_STMT*/
     LEFT OUTER JOIN (SELECT STRING_AGG(DISTINCT snnn.plcsys_ke, ', ' ORDER BY snnn.plcsys_ke) AS snnn,
                             esp_s.entr_src_id,
                             esp_s.entr_tp
                      FROM entr_fiche_plc esp_s
                           LEFT OUTER JOIN plc plc_s ON plc_s.plc_id = esp_s.entr_dst_id
                           LEFT OUTER JOIN entr_plc_plcsys epp1_s
                                           ON epp1_s.entr_src_id = plc_s.plc_id AND epp1_s.entr_tp = 'SAFETY'
                           LEFT OUTER JOIN plcsys snnn ON epp1_s.entr_dst_id = snnn.plcsys_id AND snnn.tp = 'SAFETY'
                      GROUP BY esp_s.entr_src_id, esp_s.entr_tp) ventr_fiche_plcsys_s
                     ON ventr_fiche_plcsys_s.entr_src_id = m_id AND ventr_fiche_plcsys_s.entr_tp = 'DPD_S'
                         /*SDS (attachement only)*/
     LEFT OUTER JOIN (SELECT efsds.entr_src_id,
                             efsds.entr_tp,
                             sdsdoc.*,
                             RANK()
                             OVER (PARTITION BY efsds.entr_src_id ORDER BY sdsdoc.date_of_revision DESC) AS rank_sds_attachment
                      FROM entr_fiche_sdsdoc AS efsds
                           INNER JOIN sdsdoc ON efsds.entr_dst_id = sdsdoc.sdsdoc_id AND sdsdoc.status = 'C-VALID'
                           INNER JOIN docr_sds ON docr_src_id = sdsdoc_id AND docr_tp = 'SDS_ORG'
                           INNER JOIN doc ON docr_dst_id = doc_id) ventr_fiche_sdsdoc_attachment
                     ON ventr_fiche_sdsdoc_attachment.entr_src_id = m_id
                         AND ventr_fiche_sdsdoc_attachment.entr_tp = 'SDS'
                         AND (ventr_fiche_sdsdoc_attachment.rank_sds_attachment = 1 OR
                              ventr_fiche_sdsdoc_attachment.rank_sds_attachment IS NULL)
                         /*SDS_E*/
     LEFT OUTER JOIN (SELECT docr_src_id,
                             docr_tp,
                             doc.*,
                             RANK() OVER (PARTITION BY docr_src_id ORDER BY doc.validation_dt DESC) AS rank_sdse
                      FROM docr_fiche
                           INNER JOIN doc ON docr_dst_id = doc_id AND doc.tp = 'SDS_E') ventr_fiche_sdse
                     ON ventr_fiche_sdse.docr_src_id = m_id
                         AND ventr_fiche_sdse.docr_tp = 'ATTACHMENT'
                         AND (ventr_fiche_sdse.rank_sdse = 1 OR ventr_fiche_sdse.rank_sdse IS NULL)
                         /*Simplified SDS*/
     LEFT OUTER JOIN (SELECT efsds.entr_src_id,
                             efsds.entr_tp,
                             sdsdoc.*,
                             RANK()
                             OVER (PARTITION BY efsds.entr_src_id ORDER BY sdsdoc.date_of_revision DESC) AS rank_sds_sdsdoc
                      FROM entr_fiche_sdsdoc AS efsds
                           INNER JOIN sdsdoc ON efsds.entr_dst_id = sdsdoc.sdsdoc_id AND sdsdoc.status = 'C-VALID'
                           INNER JOIN docr_sds ON docr_src_id = sdsdoc_id AND docr_tp = 'SDS'
                           INNER JOIN doc ON docr_dst_id = doc_id) ventr_fiche_sdsdoc
                     ON ventr_fiche_sdsdoc.entr_src_id = m_id
                         AND ventr_fiche_sdsdoc.entr_tp = 'SDS'
                         AND (ventr_fiche_sdsdoc.rank_sds_sdsdoc = 1 OR ventr_fiche_sdsdoc.rank_sds_sdsdoc IS NULL)
                         /*PHYSICAL_STATE*/
     LEFT OUTER JOIN (SELECT *
                      FROM entr_fiche_endpt AS efe
                           INNER JOIN endpt ON efe.entr_dst_id = endpt_id
                           INNER JOIN der_endpt ON der_endpt.der_src_id = endpt_id
                           INNER JOIN de ON der_endpt.der_dst_id = de_id
                      WHERE de_ke = 'PSTATE_USE') ventr_fiche_PHYSSTATE
                     ON ventr_fiche_PHYSSTATE.entr_src_id = m_id AND ventr_fiche_PHYSSTATE.entr_tp = 'MAT_ENDPT'
                         /*FLASHPOINT*/
     LEFT OUTER JOIN (SELECT *
                      FROM entr_fiche_endpt AS efe
                           INNER JOIN endpt ON efe.entr_dst_id = endpt_id
                           INNER JOIN der_endpt ON der_endpt.der_src_id = endpt_id
                           INNER JOIN de ON der_endpt.der_dst_id = de_id
                      WHERE de_ke = 'POINT_ECLAIR') ventr_fiche_flashpoint
                     ON ventr_fiche_flashpoint.entr_src_id = m_id AND ventr_fiche_flashpoint.entr_tp = 'MAT_ENDPT'
                         /*BOILINGPOINT*/
     LEFT OUTER JOIN (SELECT *
                      FROM entr_fiche_endpt AS efe
                           INNER JOIN endpt ON efe.entr_dst_id = endpt_id
                           INNER JOIN der_endpt ON der_endpt.der_src_id = endpt_id
                           INNER JOIN de ON der_endpt.der_dst_id = de_id
                      WHERE de_ke = 'BOILPOINT') ventr_fiche_boilingpoint
                     ON ventr_fiche_boilingpoint.entr_src_id = m_id AND ventr_fiche_boilingpoint.entr_tp = 'MAT_ENDPT'
                         /*VAPOUR PRESSURE*/
     LEFT OUTER JOIN (SELECT *
                      FROM entr_fiche_endpt AS efe
                           INNER JOIN endpt ON efe.entr_dst_id = endpt_id
                           INNER JOIN der_endpt ON der_endpt.der_src_id = endpt_id
                           INNER JOIN de ON der_endpt.der_dst_id = de_id
                      WHERE de_ke = 'VAPOUR_PRESSURE') ventr_fiche_vapour_pressure
                     ON ventr_fiche_vapour_pressure.entr_src_id = m_id AND
                        ventr_fiche_vapour_pressure.entr_tp = 'MAT_ENDPT'
                         /*CINEMATIC VISCOSITY*/
     LEFT OUTER JOIN (SELECT *
                      FROM entr_fiche_endpt AS efe
                           INNER JOIN endpt ON efe.entr_dst_id = endpt_id
                           INNER JOIN der_endpt ON der_endpt.der_src_id = endpt_id
                           INNER JOIN de ON der_endpt.der_dst_id = de_id
                      WHERE de_ke = 'CINEMATIC_VISCOSITY') ventr_fiche_cinematic_viscosity
                     ON ventr_fiche_cinematic_viscosity.entr_src_id = m_id AND
                        ventr_fiche_cinematic_viscosity.entr_tp = 'MAT_ENDPT'
                         /*PH*/
     LEFT OUTER JOIN (SELECT *
                      FROM entr_fiche_endpt AS efe
                           INNER JOIN endpt ON efe.entr_dst_id = endpt_id
                           INNER JOIN der_endpt ON der_endpt.der_src_id = endpt_id
                           INNER JOIN de ON der_endpt.der_dst_id = de_id
                      WHERE de_ke = 'PH') ventr_fiche_ph
                     ON ventr_fiche_ph.entr_src_id = m_id AND ventr_fiche_ph.entr_tp = 'MAT_ENDPT'
                         /*COV*/
     LEFT OUTER JOIN (SELECT *
                      FROM entr_fiche_endpt AS efe
                           INNER JOIN endpt ON efe.entr_dst_id = endpt_id
                           INNER JOIN der_endpt ON der_endpt.der_src_id = endpt_id
                           INNER JOIN de ON der_endpt.der_dst_id = de_id
                      WHERE de_ke = 'COV') ventr_fiche_cov
                     ON ventr_fiche_cov.entr_src_id = m_id AND ventr_fiche_cov.entr_tp = 'MAT_ENDPT'
WHERE SPLIT_PART(fiche.ns, '/', 1) LIKE :user_organisation
  AND COALESCE(fiche.active, 0) <> 0
GROUP BY m_id,
         m_ref,
         UFI.der_char1,
         marguage,
         fiche_nt,
         manufacturer,
         manufacturer_ref,
         entr_modificator.entr_dt1,
         entr_creator.entr_dt1,
         displayname,
         postalcode,
         postaladdress,
         dir_supplier.city,
         dir_supplier.country,
         entr_supplier.entr_char1,
         ventr_fiche_sdsdoc_attachment.name,
         ventr_fiche_sdsdoc_attachment.lg,
         ventr_fiche_sdsdoc_attachment.date_of_revision,
         ventr_fiche_sdsdoc_attachment.sds_valid1_dt,
         ventr_fiche_sdsdoc_attachment.sds_version,
         ventr_fiche_sdsdoc_attachment.rank_sds_attachment,
         ventr_fiche_sdse.doc_file_name,
         ventr_fiche_sdse.lg,
         ventr_fiche_sdse.doc_cr_dt,
         ventr_fiche_sdse.doc_version,
         ventr_fiche_sdse.validation_dt,
         ventr_fiche_sdse.rank_sdse,
         ventr_fiche_sdsdoc.name,
         ventr_fiche_sdsdoc.lg,
         ventr_fiche_sdsdoc.date_of_revision,
         ventr_fiche_sdsdoc.sds_version,
         ventr_fiche_sdsdoc.rank_sds_sdsdoc,
         ventr_fiche_plcsys_h.hnnn,
         ventr_fiche_plcsys_p.pnnn,
         ventr_fiche_plcsys_r.rnnn,
         ventr_fiche_plcsys_s.snnn,
         picto,
         warningtb.danger,
         Physical_State,
         ventr_fiche_flashpoint.value_operator,
         ventr_fiche_flashpoint.val,
         ventr_fiche_flashpoint.value_unit,
         ventr_fiche_boilingpoint.value_operator,
         ventr_fiche_boilingpoint.val,
         ventr_fiche_boilingpoint.value_unit,
         ventr_fiche_ph.val,
         ventr_fiche_cinematic_viscosity.val,
         ventr_fiche_cov.val,
         ventr_fiche_vapour_pressure.value_operator,
         ventr_fiche_vapour_pressure.val,
         ventr_fiche_vapour_pressure.value_unit
ORDER BY m_ref;


WITH product AS (SELECT m_id, m_ref, marguage,
                        clps, clpsh
                 FROM (SELECT m_id, m_ref, marguage,
                           /* CLP */
                              lCLP.clps,
                           /* CLP-Hnnn */
                              lCLP.clpsh
                       FROM fiche
/* Site */
                            LEFT OUTER JOIN entr_presence_fiche AS epf
                                            ON epf.entr_dst_id = m_id AND epf.entr_tp = 'MATERIAL'
                            LEFT OUTER JOIN entr_site_presence AS esp
                                            ON esp.entr_dst_id = epf.entr_src_id AND esp.entr_tp = 'MATERIAL'
                            LEFT OUTER JOIN site ON site_id = esp.entr_src_id
/* CLP */
                            LEFT OUTER JOIN
                                            (SELECT efp.entr_src_id,
                                                    STRING_AGG(DISTINCT HClassCat.plcsys_ke, ', '
                                                               ORDER BY HClassCat.plcsys_ke) AS clps,
                                                    STRING_AGG(DISTINCT plcsys.plcsys_ke, ', ' ORDER BY plcsys.plcsys_ke) AS clpsh
                                             FROM entr_fiche_plc efp
                                                  INNER JOIN plc ON efp.entr_dst_id = plc.plc_id
                                                  INNER JOIN entr_plc_plcsys epps
                                                             ON plc.plc_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
                                                  INNER JOIN plcsys HClassCat ON HClassCat.plcsys_id = epps.entr_dst_id
                                                  INNER JOIN entr_plcsys_plcsys epsps
                                                             ON epsps.entr_src_id = epps.entr_dst_id AND epsps.entr_tp =
                                                                                                         CONCAT('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
                                                  INNER JOIN plcsys
                                                             ON plcsys.plcsys_id = epsps.entr_dst_id AND plcsys.name = 'GHS_H_STMT'
                                             WHERE efp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                                             GROUP BY efp.entr_src_id) lCLP ON lCLP.entr_src_id = m_id
                       WHERE COALESCE(fiche.active, 0) <> 0
                         AND SPLIT_PART(fiche.ns, '/', 1) LIKE :user_organisation) t
                 GROUP BY m_id, m_ref, marguage, clps, clpsh),

     vlep AS (SELECT m_id AS product_id, sub_id, MIN(ers.entr_float2) AS min_vle, COUNT(reg.reg_id) AS nb_country,
                     STRING_AGG(DISTINCT de_ke, ', ' ORDER BY de_ke) AS countries
              FROM fiche
                   INNER JOIN entr_fiche_nomclit efn ON efn.entr_src_id = m_id AND efn.entr_tp = 'REAL_COMPO_SUBSTANCE'
                   INNER JOIN entr_nomclit_substances ens
                              ON ens.entr_src_id = efn.entr_dst_id AND ens.entr_tp = 'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
                   INNER JOIN substances s ON s.sub_id = ens.entr_dst_id
                   INNER JOIN entr_reg_substances ers ON ers.entr_src_id = s.sub_id
                   INNER JOIN reg ON reg.reg_id = ers.entr_dst_id AND reg.reg_ke LIKE 'OCC_EXPOSURE_LIMITS%'
                   INNER JOIN der_reg ON der_src_id = reg_id AND der_tp = 'REGULATION_COUNTRY'
                   INNER JOIN de ON de_id = der_dst_id
              WHERE COALESCE(fiche.active, 0) <> 0
                AND SPLIT_PART(fiche.ns, '/', 1) LIKE :user_organisation
              GROUP BY m_id, sub_id)
SELECT m_id, m_ref, marguage,
    /* CLP */
       product.clps,
    /* CLP - Hnnn */
       product.clpsh,
    /* composition */
       composition.conc_real_lbound_inc,
       --MIN( composition.conc_real_lbound) as concentration_min,
       composition.conc_real_ubound_inc,
       --MAX(composition.conc_real_ubound) as concentration_max,
       substances.cas, substances.ec, substances.idx, substances.naml,
       (
           EXISTS(SELECT 1
                  FROM entr_nomclit_plc
                       INNER JOIN entr_plc_plcsys ON entr_plc_plcsys.entr_src_id = entr_nomclit_plc.entr_dst_id AND
                                                     entr_plc_plcsys.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK')
                       INNER JOIN plcsys ON plcsys.plcsys_id = entr_plc_plcsys.entr_dst_id
                  WHERE entr_nomclit_plc.entr_src_id = nomclit_id
                    AND entr_nomclit_plc.entr_tp = CONCAT('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
                    AND plcsys.plcsys_ke SIMILAR TO '(Muta. 1|Carc. 1|Repr. 1|R45|R46|R49|R60|R61)%'
               )
           ) AS cmr1,
       (
           EXISTS(SELECT 1
                  FROM entr_nomclit_plc
                       INNER JOIN entr_plc_plcsys ON entr_plc_plcsys.entr_src_id = entr_nomclit_plc.entr_dst_id AND
                                                     entr_plc_plcsys.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK')
                       INNER JOIN plcsys ON plcsys.plcsys_id = entr_plc_plcsys.entr_dst_id
                  WHERE entr_nomclit_plc.entr_tp = CONCAT('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
                    AND plcsys.plcsys_ke SIMILAR TO '(Muta. 2|Carc. 2|Repr. 2|R40|R62|R63|R68)%'
               )
           ) AS cmr2,
       COALESCE(vlep.min_vle, 0) AS min_vle,
       COALESCE(vlep.nb_country, 0) AS nb_country,
       COALESCE(vlep.countries, '') AS countries,
       sCLP.name AS substance_classification_name,
       sCLP.clps AS substance_classification
FROM product
/* Composition */
     LEFT OUTER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE
                     ON ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_src_id = product.m_id AND
                        ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_tp = 'REAL_COMPO_SUBSTANCE'
     LEFT OUTER JOIN nomclit composition ON composition.nomclit_id = ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_dst_id
     LEFT OUTER JOIN ENTR_nomclit_substances ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS
                     ON ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_src_id =
                        composition.nomclit_id AND ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_tp =
                                                   'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
     LEFT OUTER JOIN substances substances
                     ON substances.sub_id = ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_dst_id

                         --                               LEFT OUTER JOIN entr_subset_substances svhc_sub ON svhc_sub.entr_dst_id = sub_id AND svhc_sub.entr_tp = 'IS_IN' AND
--                                                         svhc_sub.entr_src_id =
--                                                         (SELECT subset_id FROM subset WHERE key = 'All SVHC')
--      LEFT OUTER JOIN vlep ON vlep.product_id = product.m_id AND vlep.sub_id = substances.sub_id
--
--     LEFT OUTER JOIN entr_reg_substances registred_sub
--                      ON registred_sub.entr_dst_id = substances.sub_id AND registred_sub.entr_tp = 'SUBSTANCES' AND
--                         registred_sub.entr_src_id = 100
--      LEFT OUTER JOIN entr_reg_substances exempted_sub
--                      ON exempted_sub.entr_dst_id = substances.sub_id AND exempted_sub.entr_tp = 'SUBSTANCES' AND
--                         exempted_sub.entr_src_id = 27
/* component CLP */
     LEFT OUTER JOIN
                     (SELECT enp.entr_src_id AS sCLP_nomclit_id, plc.name,
                             STRING_AGG(DISTINCT plcsys.plcsys_ke, ', ' ORDER BY plcsys.plcsys_ke) AS clps
                      FROM entr_nomclit_plc enp
                           INNER JOIN plc ON enp.entr_dst_id = plc.plc_id
                           INNER JOIN entr_plc_plcsys epps
                                      ON plc.plc_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT', 'RISK')
                           INNER JOIN plcsys ON plcsys.plcsys_id = epps.entr_dst_id
                      WHERE enp.entr_tp = CONCAT('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
                      GROUP BY enp.entr_src_id, plc.name) sCLP ON sCLP_nomclit_id = composition.nomclit_id
-- group by
--     m_id, m_ref, marguage,
--     product.clps, product.clpsh,
--     composition.conc_real_lbound_inc  ,
--     composition.conc_real_lbound,
--     composition.conc_real_ubound_inc ,
--     composition.conc_real_ubound,
--     substances.cas, substances.ec, substances.idx, substances.naml,--, cmr1, cmr2,
--     min_vle, nb_country, countries, substance_classification_name, substance_classification
ORDER BY m_ref, marguage, substances.naml, substances.cas, substances.ec, composition.conc_real_lbound_inc,
         concentration_min, composition.conc_real_ubound_inc --, concentration_max DESC
;

-- META 2 (optimized)
WITH sCLP AS (SELECT DISTINCT enp.entr_src_id AS nomclit_id, epps.entr_tp,
                     STRING_AGG(plcsys.plcsys_ke, ', ') over (PARTITION BY enp.entr_src_id, epps.entr_tp)AS clps,
                     STRING_AGG(plc.name, ', ') over (PARTITION BY enp.entr_src_id, epps.entr_tp)AS name
              FROM entr_nomclit_plc enp
                   INNER JOIN plc ON enp.entr_dst_id = plc.plc_id
                   INNER JOIN entr_plc_plcsys epps
                              ON plc.plc_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
                   INNER JOIN plcsys ON plcsys.plcsys_id = epps.entr_dst_id
              WHERE enp.entr_tp = CONCAT('ITEM_CLASSIFICATION_SELECTION_', :REGULATORY_REFERENTIAL)
              /*GROUP BY enp.entr_src_id, epps.entr_tp*/),

     lCLP_1 AS (SELECT efp.entr_src_id AS m_id,
                       STRING_AGG(DISTINCT HClassCat.plcsys_ke, ', ' ORDER BY HClassCat.plcsys_ke) AS clps,
                       STRING_AGG(DISTINCT plcsys.plcsys_ke, ', ' ORDER BY plcsys.plcsys_ke) AS clpsh
                FROM entr_fiche_plc efp
                     INNER JOIN plc ON efp.entr_dst_id = plc.plc_id
                     INNER JOIN entr_plc_plcsys epps
                                ON plc.plc_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
                     INNER JOIN plcsys HClassCat ON HClassCat.plcsys_id = epps.entr_dst_id
                     INNER JOIN entr_plcsys_plcsys epsps ON epsps.entr_src_id = epps.entr_dst_id AND
                                                            epsps.entr_tp =
                                                            CONCAT('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
                     INNER JOIN plcsys
                                ON plcsys.plcsys_id = epsps.entr_dst_id AND plcsys.name = 'GHS_H_STMT'
                WHERE efp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                GROUP BY efp.entr_src_id),
    /*   lCLP AS (SELECT epf.entr_dst_id AS m_id,
                    /* CLP */
                       lCLP.clps,
                    /* CLP-Hnnn */
                       lCLP.clpsh
                FROM
                    /* Site */
                    entr_presence_fiche AS epf
                    INNER JOIN entr_site_presence AS esp
                               ON esp.entr_dst_id = epf.entr_src_id AND esp.entr_tp = 'MATERIAL'
                    INNER JOIN site ON site_id = esp.entr_src_id
                                   /* CLP */
                    INNER JOIN (SELECT efp.entr_src_id AS m_id,
                                       STRING_AGG(DISTINCT HClassCat.plcsys_ke, ', '
                                                  ORDER BY HClassCat.plcsys_ke) AS clps,
                                       STRING_AGG(DISTINCT plcsys.plcsys_ke, ', ' ORDER BY plcsys.plcsys_ke) AS clpsh
                                FROM entr_fiche_plc efp
                                     INNER JOIN plc ON efp.entr_dst_id = plc.plc_id
                                     INNER JOIN entr_plc_plcsys epps
                                                ON plc.plc_id = epps.entr_src_id AND epps.entr_tp IN ('HAZARD_CLASS_CAT')
                                     INNER JOIN plcsys HClassCat ON HClassCat.plcsys_id = epps.entr_dst_id
                                     INNER JOIN entr_plcsys_plcsys epsps ON epsps.entr_src_id = epps.entr_dst_id AND
                                                                            epsps.entr_tp =
                                                                            CONCAT('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
                                     INNER JOIN plcsys
                                                ON plcsys.plcsys_id = epsps.entr_dst_id AND plcsys.name = 'GHS_H_STMT'
                                WHERE efp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                                GROUP BY efp.entr_src_id) lCLP ON lCLP.m_id = epf.entr_dst_id
                WHERE epf.entr_tp = 'MATERIAL'),
       product AS (SELECT m_id, m_ref, marguage,
                          clps, clpsh
                   FROM fiche
                        LEFT JOIN LCLP USING (m_id)
                   WHERE COALESCE(fiche.active, 0) <> 0
                     AND SPLIT_PART(fiche.ns, '/', 1) = :user_organisation
                   GROUP BY m_id, m_ref, marguage, clps, clpsh),*/
     vlep AS (SELECT ers.entr_dst_id AS sub_id, MIN(ers.entr_float2) AS min_vle, COUNT(reg.reg_id) AS nb_country,
                     STRING_AGG(DISTINCT de_ke, ', ' ORDER BY de_ke) AS countries
              FROM entr_reg_substances ers
                   INNER JOIN reg ON reg.reg_id = ers.entr_dst_id
                   INNER JOIN der_reg ON der_src_id = reg_id AND der_tp = 'REGULATION_COUNTRY'
                   INNER JOIN de ON de_id = der_dst_id
              WHERE reg.reg_ke LIKE 'OCC_EXPOSURE_LIMITS%'
              GROUP BY sub_id),
     all_svhc AS (SELECT subset_id, entr_dst_id AS sub_id
                  FROM subset
                       INNER JOIN
                       entr_subset_substances svhc_sub
                       ON svhc_sub.entr_src_id = subset_id AND svhc_sub.entr_tp = 'IS_IN'
                  WHERE KEY = 'All SVHC'),
     composition AS (SELECT ENTR_fiche_nomclit.entr_src_id AS m_id,
                         /* composition */
                            composition.nomclit_id,
                            composition.conc_real_lbound_inc,
                            MIN(composition.conc_real_lbound) AS concentration_min,
                            composition.conc_real_ubound_inc,
                            MAX(composition.conc_real_ubound) AS concentration_max,
                            substances.sub_id,
                            substances.cas, substances.ec, substances.idx, substances.naml
                     FROM ENTR_fiche_nomclit
                          LEFT JOIN nomclit composition ON composition.nomclit_id = ENTR_fiche_nomclit.entr_dst_id
                          LEFT JOIN ENTR_nomclit_substances ON ENTR_nomclit_substances.entr_src_id =
                                                               composition.nomclit_id AND
                                                               ENTR_nomclit_substances.entr_tp =
                                                               'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
                          LEFT JOIN substances ON substances.sub_id = ENTR_nomclit_substances.entr_dst_id
                     WHERE ENTR_fiche_nomclit.entr_tp = 'REAL_COMPO_SUBSTANCE'
                       AND SPLIT_PART(composition.ns, '/', 1) = :user_organisation
                     GROUP BY ENTR_fiche_nomclit.entr_src_id, composition.nomclit_id, composition.conc_real_lbound_inc,
                              composition.conc_real_ubound_inc, substances.sub_id,
                              substances.cas, substances.ec, substances.idx, substances.naml)
SELECT m_id, m_ref, marguage,
       lCLP_1.clps, lCLP_1.clpsh,
    /* composition */
       composition.conc_real_lbound_inc,
       composition.concentration_min,
       composition.conc_real_ubound_inc,
       composition.concentration_max,
       COALESCE(vlep.min_vle, 0) AS min_vle,
       COALESCE(vlep.nb_country, 0) AS nb_country,
       COALESCE(vlep.countries, '') AS countries,
       composition.cas, composition.ec, composition.idx, composition.naml,
       CASE WHEN sCLP.entr_tp = 'HAZARD_CLASS_CAT' THEN sCLP.clps ELSE NULL END AS substance_classification,
       CASE WHEN sCLP.entr_tp = 'HAZARD_CLASS_CAT' THEN sCLP.name ELSE NULL END AS substance_classification_name,
       CASE
           WHEN UPPER(sCLP.clps) SIMILAR TO UPPER('%(Muta. 1|Carc. 1|Repr. 1|R45|R46|R49|R60|R61)%') THEN TRUE
           ELSE FALSE END AS cmr1,
       CASE
           WHEN UPPER(sCLP.clps) SIMILAR TO UPPER('%(Muta. 2|Carc. 2|Repr. 2|R40|R62|R63|R68)%') THEN TRUE
           ELSE FALSE END AS cmr2

FROM fiche
     LEFT JOIN composition USING (m_id)
     LEFT JOIN lCLP_1 USING (m_id)
     LEFT JOIN sCLP USING (nomclit_id)
     LEFT JOIN vlep USING (sub_id)
     LEFT JOIN all_svhc USING (sub_id)

WHERE COALESCE(fiche.active, 0) <> 0
  AND SPLIT_PART(fiche.ns, '/', 1) = :user_organisation

ORDER BY m_ref, marguage, composition.naml, composition.cas, composition.ec, composition.conc_real_lbound_inc,
         composition.conc_real_ubound_inc DESC
;



SELECT m_id, m_ref, marguage,
    /* CLP */
       product.clps,
    /* CLP - Hnnn */
       product.clpsh,
    /* composition */
       composition.conc_real_lbound_inc,
--        MIN(composition.conc_real_lbound) AS concentration_min,
       composition.conc_real_ubound_inc,
--        MAX(composition.conc_real_ubound) AS concentration_max,
--        COALESCE(vlep.min_vle, 0) AS min_vle,
--        COALESCE(vlep.nb_country, 0) AS nb_country,
--        COALESCE(vlep.countries, '') AS countries,
       substances.cas, substances.ec, substances.idx, substances.naml,
       CASE WHEN sCLP.entr_tp = 'HAZARD_CLASS_CAT' THEN sCLP.clps ELSE NULL END AS substance_classification,
       CASE WHEN sCLP.entr_tp = 'HAZARD_CLASS_CAT' THEN sCLP.name ELSE NULL END AS substance_classification_name,
       CASE
           WHEN UPPER(sCLP.clps) SIMILAR TO UPPER('%(Muta. 1|Carc. 1|Repr. 1|R45|R46|R49|R60|R61)%') THEN TRUE
           ELSE FALSE END AS cmr1,
       CASE
           WHEN UPPER(sCLP.clps) SIMILAR TO UPPER('%(Muta. 2|Carc. 2|Repr. 2|R40|R62|R63|R68)%') THEN TRUE
           ELSE FALSE END AS cmr2
FROM product
     LEFT JOIN       ENTR_fiche_nomclit
                     ON ENTR_fiche_nomclit.entr_src_id = product.m_id AND
                        ENTR_fiche_nomclit.entr_tp = 'REAL_COMPO_SUBSTANCE'
     LEFT JOIN       nomclit composition ON composition.nomclit_id = ENTR_fiche_nomclit.entr_dst_id
     LEFT JOIN       ENTR_nomclit_substances
                     ON ENTR_nomclit_substances.entr_src_id =
                        composition.nomclit_id AND ENTR_nomclit_substances.entr_tp =
                                                   'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
     LEFT OUTER JOIN substances
                     ON substances.sub_id = ENTR_nomclit_substances.entr_dst_id

     LEFT OUTER JOIN sCLP USING (nomclit_id)
     LEFT OUTER JOIN vlep USING (sub_id)
     LEFT OUTER JOIN all_svhc USING (sub_id)
;
-- GROUP BY m_id, m_ref, marguage,
--          product.clps, product.clpsh,
--          composition.conc_real_lbound_inc,
--          composition.conc_real_lbound,
--          composition.conc_real_ubound_inc,
--          composition.conc_real_ubound,
--          substances.cas, substances.ec, substances.idx, substances.naml,--, cmr1, cmr2,
--          min_vle, nb_country, countries, substance_classification_name, substance_classification, sCLP.name, sCLP.clps
-- ORDER BY m_ref, marguage, substances.naml, substances.cas, substances.ec, composition.conc_real_lbound_inc, concentration_min, composition.conc_real_ubound_inc , concentration_max DESC
-- ;

-- META 3.
-----------------------------------------------------------------

WITH product AS (SELECT m_id, m_ref, marguage
                 FROM (SELECT m_id, m_ref, marguage
                       FROM fiche
                            LEFT OUTER JOIN entr_presence_fiche AS epf
                                            ON epf.entr_dst_id = m_id AND epf.entr_tp = 'MATERIAL'
                       WHERE COALESCE(fiche.active, 0) <> 0
                         AND (fiche.ns LIKE :user_organisation || '/%' OR fiche.ns = :user_organisation)) t)
SELECT DISTINCT substances.cas, substances.ec, substances.idx, substances.naml
FROM product
/* Composition */
     LEFT OUTER JOIN ENTR_fiche_nomclit ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE
                     ON ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_src_id = product.m_id AND
                        ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_tp = 'REAL_COMPO_SUBSTANCE'
     LEFT OUTER JOIN nomclit composition ON composition.nomclit_id = ENTR_fiche_nomclit_REAL_COMPO_SUBSTANCE.entr_dst_id
     LEFT OUTER JOIN ENTR_nomclit_substances ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS
                     ON ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_src_id =
                        composition.nomclit_id AND ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_tp =
                                                   'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
     LEFT OUTER JOIN substances substances
                     ON substances.sub_id = ENTR_nomclit_substances_NOMENCLATURE_ITEM_FOR_MATERIAL_IS.entr_dst_id
ORDER BY substances.cas, substances.ec, substances.idx, substances.naml DESC;


SELECT m_id, m_ref, marguage,
/* Fournisseur */
       SUPPLIER.displayname,
/* CLP */
       lCLP.clps,
/* DPD */
       lDPD.dpds
/* FDS */
FROM fiche
/* Fournisseur */
     LEFT OUTER JOIN entr_fiche_dir lSUPPLIER ON lSUPPLIER.entr_src_id = m_id AND lSUPPLIER.entr_tp = 'SUPPLIER' LEFT OUTER JOIN dir SUPPLIER on supplier.dir_id = lSUPPLIER.entr_dst_id
/* CLP */
     LEFT OUTER JOIN
                     (SELECT efp.entr_src_id, string_agg(DISTINCT plcsys.plcsys_ke, ' ' ORDER BY plcsys.plcsys_ke) as clps
                      FROM entr_fiche_plc efp
                           INNER JOIN entr_plc_plcsys epps on epps.entr_src_id = efp.entr_dst_id and epps.entr_tp in ('HAZARD_CLASS_CAT', 'H_STMT_SUPP')
                           INNER JOIN entr_plcsys_plcsys epsps on epsps.entr_src_id = epps.entr_dst_id AND epsps.entr_tp = CONCAT('GHS_H_STMT_', :REGULATORY_REFERENTIAL)
                           INNER JOIN plcsys on plcsys.plcsys_id = epsps.entr_dst_id
                      WHERE efp.entr_tp = CONCAT('CLP_S_', :REGULATORY_REFERENTIAL)
                      GROUP BY efp.entr_src_id) lCLP on lCLP.entr_src_id = m_id
/* DPD */
     LEFT OUTER JOIN
                     (SELECT efp.entr_src_id, string_agg(DISTINCT plcsys.plcsys_ke, ' ' ORDER BY plcsys.plcsys_ke) as dpds
                      FROM entr_fiche_plc efp
                           INNER JOIN entr_plc_plcsys epps on epps.entr_src_id = efp.entr_dst_id and epps.entr_tp = 'RISK'
                           INNER JOIN plcsys on plcsys.plcsys_id = epps.entr_dst_id and plcsys.name = 'DSD'
                      WHERE efp.entr_tp = 'DPD_S'
                      GROUP BY efp.entr_src_id) lDPD on lDPD.entr_src_id = m_id
WHERE COALESCE(fiche.active, 0) <> 0
  AND SPLIT_PART(fiche.ns, '/', 1) = :user_organisation



