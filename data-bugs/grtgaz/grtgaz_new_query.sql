-- META 2 (optimized)
WITH vlep AS (SELECT ers.entr_dst_id AS sub_id, MIN(ers.entr_float2) AS min_vle, COUNT(reg.reg_id) AS nb_country,
                     STRING_AGG(DISTINCT de_ke, ', ' ORDER BY de_ke) AS countries
              FROM entr_reg_substances ers
                   INNER JOIN reg ON reg.reg_id = ers.entr_dst_id
                   INNER JOIN der_reg ON der_src_id = reg_id AND der_tp = 'REGULATION_COUNTRY'
                   INNER JOIN de ON de_id = der_dst_id
              WHERE reg.reg_ke LIKE 'OCC_EXPOSURE_LIMITS%'
              GROUP BY sub_id),
     composition AS (SELECT ENTR_fiche_nomclit.entr_src_id AS m_id,
                         /* composition */
                            composition.conc_real_lbound_inc,
                            MIN(composition.conc_real_lbound) AS concentration_min,
                            composition.conc_real_ubound_inc,
                            MAX(composition.conc_real_ubound) AS concentration_max,
                            substances.sub_id,
                            substances.cas, substances.ec, substances.idx, substances.naml
                     FROM ENTR_fiche_nomclit
                          INNER JOIN nomclit composition ON composition.nomclit_id = ENTR_fiche_nomclit.entr_dst_id
                          INNER JOIN ENTR_nomclit_substances ON ENTR_nomclit_substances.entr_src_id =
                                                                composition.nomclit_id AND
                                                                ENTR_nomclit_substances.entr_tp =
                                                                'NOMENCLATURE_ITEM_FOR_MATERIAL_IS'
                          INNER JOIN substances ON substances.sub_id = ENTR_nomclit_substances.entr_dst_id
                     WHERE ENTR_fiche_nomclit.entr_tp = 'REAL_COMPO_SUBSTANCE'
                       AND SPLIT_PART(composition.ns, '/', 1) = :user_organisation
                     GROUP BY ENTR_fiche_nomclit.entr_src_id, composition.conc_real_lbound_inc,
                              composition.conc_real_ubound_inc, substances.sub_id,
                              substances.cas, substances.ec, substances.idx, substances.naml)
SELECT m_id, m_ref, marguage,
    /* composition */
       composition.conc_real_lbound_inc,
       composition.concentration_min,
       composition.conc_real_ubound_inc,
       composition.concentration_max,
       COALESCE(vlep.min_vle, 0) AS min_vle,
       COALESCE(vlep.nb_country, 0) AS nb_country,
       COALESCE(vlep.countries, '') AS countries,
       composition.cas, composition.ec, composition.idx, composition.naml
FROM fiche
     LEFT JOIN composition USING (m_id)
     LEFT JOIN vlep USING (sub_id)

WHERE COALESCE(fiche.active, 0) <> 0
  AND SPLIT_PART(fiche.ns, '/', 1) = :user_organisation

ORDER BY m_ref, marguage, composition.naml, composition.cas, composition.ec, composition.conc_real_lbound_inc,
         composition.conc_real_ubound_inc DESC
;
