/* viewTp */
    -- {"entity":"Endpt","filter":{"op":"and","criterias":[{"op":"has","link_type":"EntrStudyEndpt","link_constraint":{"op":"=","field":"entrTp","value":"ENDPT"},"element_entity":"Study","element_constraint":{"op":"and","criterias":[{"op":"has","link_type":"DerStudy","link_constraint":{"op":"=","field":"derTp","value":"STUDY_TP"},"element_entity":"De","element_constraint":{"op":"like","field":"deKe","value":"GENOTOXIC_IN_VIVO"}},{"op":"or","criterias":[{"op":"has","link_type":"EntrStudyInci","link_constraint":{"op":"=","field":"entrTp","value":"INCI"},"element_entity":"Inci","element_constraint":{"op":"=","field":"inciId","value":43033}},{"op":"has","link_type":"EntrStudySubstances","link_constraint":{"op":"=","field":"entrTp","value":"STUDIED"},"element_entity":"Substance","element_constraint":{"op":"=","field":"subId","value":3473}}]}]}}]},"fields":[{"name":"endptId"},{"name":"endptXml"},{"name":"valueOperator"},{"name":"val"},{"name":"allStudies","filter":{"op":"=","field":"entrTp","value":"ENDPT"},"fields":[{"name":"entrId"},{"name":"entrTp"},{"name":"src","fields":[{"name":"studyId"},{"name":"ns"},{"name":"conclusion"},{"name":"allDes","fields":[{"name":"entrId"},{"name":"entrTp"},{"name":"dst","fields":[{"name":"deKe"}]}]}]}]},{"columnCfg":{"text":"Subendpoint name"}},{"columnCfg":{"text":"Gender"}},{"columnCfg":{"text":"Operator"}},{"columnCfg":{"text":"Value"}},{"columnCfg":{"text":"Unit"}}]}

/* EQL */
WITH study_substances AS (SELECT * FROM entr_study_substances WHERE entr_tp = 'STUDIED' AND entr_dst_id = 3473),
     study_incis AS (SELECT * FROM anx.entr_study_inci WHERE entr_tp = 'INCI' AND entr_dst_id = 43033)
SELECT endpt_id, study_id, endpt_tp, endpt_name, endpt_li, de_ke, study_name
FROM endpt
     INNER JOIN entr_study_endpt ese ON endpt.endpt_id = ese.entr_dst_id and ese.entr_tp = 'ENDPT'
     INNER JOIN study s ON ese.entr_src_id = s.study_id
     INNER JOIN der_study ON s.study_id = der_study.der_src_id and der_tp = 'STUDY_TP'
     INNER JOIN de ON der_study.der_dst_id = de.de_id and de_ke = 'GENOTOXIC_IN_VIVO'
WHERE EXISTS(SELECT 1 FROM study_incis ei WHERE ei.entr_src_id = s.study_id)
   OR EXISTS(SELECT 1 FROM study_substances ei WHERE ei.entr_src_id = s.study_id)
ORDER BY endpt_name;


/* EQL Endpt */
select * from endpt where endpt_id = 117162;

/* viewTp Endpt */

-- var model = {
--     entity: "Endpt",
--     fields: ['*'],
--     filter: {equals: ['endptId', 117162]}
-- };