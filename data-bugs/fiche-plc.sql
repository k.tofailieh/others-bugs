SELECT *
FROM plcsys
WHERE plcsys_id = 172;

-- MEK = 253586
-- Flora test = 556286
SELECT plcsys.*
FROM entr_fiche_plc
         INNER JOIN entr_plc_plcsys epp
                    ON entr_fiche_plc.entr_dst_id = epp.entr_src_id AND entr_fiche_plc.entr_src_id = 253586
         INNER JOIN plcsys ON epp.entr_dst_id = plcsys.plcsys_id


SELECT *
FROM entr_fiche_plc
WHERE entr_src_id = 253586

SELECT m_id, m_ref, active
FROM fiche
WHERE m_ref LIKE 'flora%'

SELECT entr_src_id, m_ref
FROM entr_fiche_plc
         INNER JOIN fiche f ON entr_fiche_plc.entr_src_id = f.m_id

SELECT
       de_ke
     , entr_nomclit_plc.entr_src_id
     , entr_nomclit_plc.entr_dst_id
     ,entr_nomclit_plc.entr_tp as enpTp
     , efn.entr_dst_id
     , epsps.entr_tp
     , plcsys.*

FROM entr_fiche_nomclit efn
         INNER JOIN entr_nomclit_plc ON efn.entr_dst_id = entr_nomclit_plc.entr_src_id  AND entr_nomclit_plc.entr_tp IN (('ITEM_CLASSIFICATION_SELECTION_CLP' || cast('10' as text)), 'SUBSTANCE_DSD_SELECTION')
         INNER JOIN entr_plc_plcsys epps ON epps.entr_src_id = entr_nomclit_plc.entr_dst_id
         INNER JOIN entr_plcsys_plcsys epsps ON epps.entr_dst_id = epsps.entr_src_id
         INNER JOIN plcsys ON epsps.entr_dst_id = plcsys.plcsys_id AND plcsys.name = 'GHS_H_STMT'  AND  epsps.entr_tp LIKE ('%CLP' || cast(:ATP_version as text))
         INNER JOIN der_plc dp ON dp.der_src_id = entr_nomclit_plc.entr_dst_id AND dp.der_tp = 'VALID_FOR_REGULATORY'
         INNER JOIN de ON dp.der_dst_id = de.de_id AND de_tp = 'REGULATORY_REFERENTIAL' AND de_ke = ('CLP' || cast(:ATP_version as text))
WHERE efn.entr_src_id = 253586;


insert into entr_nomclit_plc(entr_id, entr_src_id, entr_dst_id, entr_tp) values (nextval('seq_entr_nomclit_plc_id'), 2745918, 479004, 'ITEM_CLASSIFICATION_SELECTION_CLP10' )

