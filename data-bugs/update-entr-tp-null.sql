/* update entr_tp=null */

-- entr_fiche_nomclit: Check in detail
UPDATE entr_fiche_nomclit
SET entr_tp = CASE
                  WHEN f1_tp = 'PRODUCT' AND f2_tp IN ('RAW_MATERIAL', 'MAT', 'MASTER_RAW_MATERIAL') THEN 'DIRECT_COMPO'
                  WHEN f1_tp = 'PRODUCT' AND f2_tp IN ('ALLERGEN', 'INGREDIENT', 'PERFUME_COMPONENT') THEN 'REAL_COMPO'
                  WHEN f1_tp = 'RAW_MATERIAL' AND f2_tp IN ('ALLERGEN', 'INGREDIENT') THEN 'DIRECT_COMPO'
    END,
    entr_co = coalesce(entr_co, '') || ', entr_tp updated because of null value.'
FROM (SELECT efn.entr_id, f.tp AS f1_tp, f2.tp AS f2_tp
      FROM entr_fiche_nomclit efn
               INNER JOIN fiche f ON efn.entr_src_id = f.m_id
               INNER JOIN nomclit n ON efn.entr_dst_id = n.nomclit_id
               INNER JOIN entr_nomclit_fiche ON n.nomclit_id = entr_nomclit_fiche.entr_src_id
               INNER JOIN fiche f2 ON f2.m_id = entr_nomclit_fiche.entr_dst_id
      WHERE efn.entr_tp IS NULL) compo
WHERE entr_fiche_nomclit.entr_id = compo.entr_id;


-- entr_nomclit_fiche: Set to "NOMENCLATURE_ITEM_FOR_FICHE" which is the generic value
UPDATE entr_nomclit_fiche
SET entr_tp = 'NOMENCLATURE_ITEM_FOR_FICHE',
    entr_co = coalesce(entr_co, '') || ', entr_tp updated because of null value.'
WHERE entr_tp IS NULL;

-- entr_phractxtke_phractxt: Set to TRANSLATION
SELECT tps.do_as_nolog(
               $$UPDATE entr_phractxtke_phractxt SET entr_tp = 'TRANSLATION', entr_co = coalesce(entr_co, '') || ', entr_tp updated because of null value.' WHERE entr_tp IS NULL; $$,
               $$UPDATE entr_phractxtke_phractxt SET entr_tp = 'TRANSLATION', entr_co = coalesce(entr_co, '') || ', entr_tp updated because of null value.' WHERE entr_tp IS NULL;$$,-- AND tpstill > '9999-12-30'
               'nolog_password',
               'mmapar',
               'Correct entr_tp = null for entr_phractxtke_phractxt.'),
       "RunOnMaster"();

-- entr_sdsdoc_sdschap: Set to "" (empty)
SELECT tps.do_as_nolog(
               $$UPDATE entr_sdsdoc_sdschap SET entr_tp = '', entr_co = coalesce(entr_co, '') || ', entr_tp updated because of null value.' WHERE entr_tp IS NULL;$$,
               $$UPDATE entr_sdsdoc_sdschap SET entr_tp = '', entr_co = coalesce(entr_co, '') || ', entr_tp updated because of null value.' WHERE entr_tp IS NULL;$$,-- AND tpstill > '9999-12-30'
               'nolog_password',
                 'mmapar',
               'Correct entr_tp = null for entr_sdsdoc_sdschap.'),
       "RunOnMaster"();

-- entr_signature_msg: Delete all (we don't care)
DELETE
FROM entr_signature_msg
WHERE entr_tp IS NULL;

-- entr_study_endpt: Delete all (we don't care)
DELETE
FROM entr_study_endpt
WHERE entr_tp IS NULL;




