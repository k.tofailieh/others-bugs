SELECT DISTINCT enf.entr_tp, RM.tp, efn_RM.entr_tp, enf_RM.entr_tp, ING.tp
FROM entr_fiche_nomclit efn
     INNER JOIN nomclit ON efn.entr_dst_id = nomclit.nomclit_id
     INNER JOIN entr_nomclit_fiche enf ON nomclit.nomclit_id = enf.entr_src_id
     INNER JOIN fiche RM ON enf.entr_dst_id = RM.m_id
     INNER JOIN entr_fiche_nomclit efn_RM ON efn_RM.entr_src_id = RM.m_id
     INNER JOIN entr_nomclit_fiche enf_RM ON efn_RM.entr_dst_id = enf_RM.entr_src_id
     INNER JOIN fiche ING ON enf_RM.entr_dst_id = ING.m_id

WHERE efn.entr_src_id = 570998;


SELECT DISTINCT entr_tp
FROM entr_fiche_nomclit
WHERE entr_src_id = 570998;


SELECT tpsfrom
FROM fiche
WHERE m_id = 570998;

SELECT MIN(tr)
FROM (SELECT incitranslation_id,
             STRING_AGG(tuv_seg, '') FILTER ( WHERE country = 'US' ) OVER (PARTITION BY incitranslation_id) tr
      FROM anx.incitranslation) trans
GROUP BY tr;


SELECT DISTINCT country
FROM anx.incitranslation;

UPDATE doc
SET st = 'VALID'
WHERE doc_id = 912498;

SELECT st
FROM doc
WHERE doc_id = 912498;