select
    coalesce(f.m_id, p.part_id)  AS related_object_id,
    COALESCE(f.m_ref,  p.name_1) AS related_object_name,
    coalesce( f.tp, p.tp) AS related_object_type,
    s.study_id as entity_id,
    s.study_id as id,
    s.study_xml as study_details,
    study_name as name,
    test_type as entity_tp,
    s.cr_dt as creation_date,
    "range".ke as test_range,
    brand.ke as test_brand,
    status.ke as test_status,
    general_type.ke  as test_general_type,
    subtype.ke as test_subtype,
    project.li as related_project_name
from
    study s
    -- join related fiche
    left join entr_fiche_study efs on efs.entr_dst_id =s.study_id and efs.entr_tp ='TEST'
    left join fiche f on efs.entr_src_id =f.m_id
    -- join related packaging
    left join entr_study_part esp on esp.entr_src_id =s.study_id and esp.entr_tp ='TESTED_PACKAGING'
    left join part p on efs.entr_id is null and esp.entr_dst_id =p.part_id
    -- join related status
    left join entr_study_lov status_link on status_link.entr_src_id =s.study_id and status_link.entr_tp ='PAILLASSE_TEST_STATE'
    left join lov status on status_link.entr_dst_id=status.lov_id
    -- join related brand
    left join entr_study_lov brand_link on brand_link.entr_src_id =s.study_id and brand_link.entr_tp ='TEST_BRAND'
    left join lov brand on brand_link.entr_dst_id=brand.lov_id
    -- join related type
    left join entr_study_lov range_link on range_link.entr_src_id =s.study_id and range_link.entr_tp ='TEST_RANGE'
    left join lov range on range_link.entr_dst_id=range.lov_id
    -- join related general type
    left join entr_study_lov general_type_link on general_type_link.entr_src_id =s.study_id and general_type_link.entr_tp ='TEST_GENERAL_TYPE'
    left join lov general_type on general_type_link.entr_dst_id=general_type.lov_id
    -- join related general type
    left join entr_study_lov subtype_link on subtype_link.entr_src_id =s.study_id and subtype_link.entr_tp ='TEST_SUBTYPE'
    left join lov subtype on subtype_link.entr_dst_id=subtype.lov_id
    -- join related project
    left join entr_project_study project_link on project_link.entr_dst_id =s.study_id and project_link.entr_tp ='CONCERNS'
    left join project on project_link.entr_src_id=project.project_id
where
    s.test_type = 'TEST'
--     and (s.study_name ilike :searchValue
--     or f.m_ref ilike :searchValue
--     or p.name_1 ilike :searchValue)
    and (p.part_id is not null or f.m_id is not null)
    --order by related_object_name,name
    ORDER BY study_id