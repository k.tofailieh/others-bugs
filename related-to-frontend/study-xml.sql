SELECT CASE
           WHEN f.m_id IS NOT NULL THEN f.m_id
           ELSE p.part_id
           END                                                     AS related_object_id,
       CASE
           WHEN f.m_ref IS NOT NULL THEN f.m_ref
           ELSE p.name_1
           END                                                     AS related_object_name,
       CASE
           WHEN f.tp IS NOT NULL THEN f.tp
           ELSE p.tp
           END                                                     AS related_object_type,
       s.study_id                                                  AS entity_id,
       s.study_id                                                  AS id,
       s.study_xml::jsonb ->> 'actualDeliveryDate'     AS actualDeliveryDate,
       s.study_xml::jsonb ->> 'estimatedDeliveryDate'  AS estimatedDeliveryDate,
       study_name                                                  AS name,
       test_type                                                   AS entity_tp,
       s.cr_dt                                                     AS creation_date,
       "range".ke                                                  AS test_range,
       brand.ke                                                    AS test_brand,
       status.ke                                                   AS test_status,
       general_type.ke                                             AS test_general_type,
       subtype.ke                                                  AS test_subtype,
       project.li                                                  AS related_project_name
FROM study s
         -- join related fiche
         LEFT JOIN entr_fiche_study efs ON efs.entr_dst_id = s.study_id AND efs.entr_tp = 'TEST'
         LEFT JOIN fiche f ON efs.entr_src_id = f.m_id
    -- join related packaging
         LEFT JOIN entr_study_part esp ON esp.entr_src_id = s.study_id AND esp.entr_tp = 'TESTED_PACKAGING'
         LEFT JOIN part p ON esp.entr_dst_id = p.part_id
    -- join related status
         LEFT JOIN entr_study_lov status_link
                   ON status_link.entr_src_id = s.study_id AND status_link.entr_tp = 'PAILLASSE_TEST_STATE'
         LEFT JOIN lov status ON status_link.entr_dst_id = status.lov_id
    -- join related brand
         LEFT JOIN entr_study_lov brand_link
                   ON brand_link.entr_src_id = s.study_id AND brand_link.entr_tp = 'TEST_BRAND'
         LEFT JOIN lov brand ON brand_link.entr_dst_id = brand.lov_id
    -- join related type
         LEFT JOIN entr_study_lov range_link
                   ON range_link.entr_src_id = s.study_id AND range_link.entr_tp = 'TEST_RANGE'
         LEFT JOIN lov range ON range_link.entr_dst_id = range.lov_id
    -- join related general type
         LEFT JOIN entr_study_lov general_type_link
                   ON general_type_link.entr_src_id = s.study_id AND general_type_link.entr_tp = 'TEST_GENERAL_TYPE'
         LEFT JOIN lov general_type ON general_type_link.entr_dst_id = general_type.lov_id
    -- join related general type

         LEFT JOIN entr_study_lov subtype_link
                   ON subtype_link.entr_src_id = s.study_id AND subtype_link.entr_tp = 'TEST_SUBTYPE'
         LEFT JOIN lov subtype ON subtype_link.entr_dst_id = subtype.lov_id
    -- join related project
         LEFT JOIN entr_project_study project_link
                   ON project_link.entr_dst_id = s.study_id AND project_link.entr_tp = 'CONCERNS'
         LEFT JOIN project ON project_link.entr_src_id = project.project_id
WHERE upper(s.test_type) = 'TEST'
-- and (s.study_name ilike :searchValue
--   or f.m_ref ilike :searchValue
--   or p.name_1 ilike :searchValue)
--   order by related_object_name,name
    ORDER BY nullif(s.study_xml::jsonb ->> :actual_date, '') DESC NULLS LAST
    -- ORDER BY  nullif(s.study_xml::jsonb ->> 'estimatedDeliveryDate', '') desc NULLS LAST
;


select m_ref, cas,  from fiche where tp = '' and cust_id is not null

select m_ref, cas from fiche where upper(m_ref) = upper('1,2-HeXAnediol');

select naml, substances.cas, inciname from substances
inner join anx.entr_substances_inci on  sub_id = entr_substances_inci.entr_src_id
inner join anx.inci on entr_substances_inci.entr_dst_id = inci.inci_id
where substances.cas = '6920-22-5';

select * from anx.inci where inciname is not null; upper(inciname) like ('%1,2-HeXAnediol%')