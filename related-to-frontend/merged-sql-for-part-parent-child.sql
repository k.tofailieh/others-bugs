SELECT mainPart.part_id        AS p_part_id,
       mainPart.active         AS p_active,
       mainPart.version_label  AS p_version_label,
       mainPart.version_tree   AS p_version_tree,
       mainPart.name_1         AS p_packagename,
       mainPart.reference_1    AS p_packagecode,
       mainPart.weightmes_1    AS p_capacity,
       mainPart.weightmes_2    AS p_weight,
       mainPart.weightunit_1   AS p_capacityunit,
       mainPart.weightunit_2   AS p_weightunit,
       supplier.displayname    AS p_packagingsuppliername,
       lgroup.entr_char1       AS p_packagingsuppliercode,
       RESPONSIBLE.dir_id      AS p_dir_id,
       RESPONSIBLE.displayname AS p_dir_id,

       child.*


FROM part mainPart
         LEFT JOIN entr_part_dir lsup ON lsup.entr_src_id = mainPart.part_id
    AND lsup.entr_tp = 'SUPPLIER'
         LEFT JOIN dir supplier ON supplier.dir_id = lsup.entr_dst_id
         LEFT JOIN entr_dir_dir lgroup ON lgroup.entr_dst_id = supplier.dir_id
    AND lgroup.entr_tp = 'SUPPLIER'
         LEFT OUTER JOIN entr_part_dir RESPONSIBLE_LINK ON RESPONSIBLE_LINK.entr_src_id = mainPart.part_id
    AND RESPONSIBLE_LINK.entr_tp = 'RESPONSIBLE_PERSON'
         LEFT OUTER JOIN dir RESPONSIBLE ON RESPONSIBLE.dir_id = RESPONSIBLE_LINK.entr_dst_id
         LEFT JOIN (SELECT *
                    FROM entr_project_part
                             INNER JOIN project ON (
                            (
                                (
                                        nullif(nullif(project.ns, '*'), '') IS NULL
                                        OR split_part(project.ns, '/', 1) = 'EcoMundo'
                                    )
                                )
                            AND (
                                        entr_project_part.entr_src_id = project.project_id
                                    AND project.tp = 'PROJECT_COSMETIC_PACKAGING'
                                )
                        )
                    WHERE entr_project_part.entr_tp = 'CONCERNS') project ON project.entr_dst_id = mainPart.part_id
    AND project.entr_tp = 'CONCERNS'
         LEFT JOIN entr_project_project ON entr_project_project.entr_dst_id = project.project_id
    AND entr_project_project.entr_tp = 'CONCERNS'
         LEFT JOIN public.project AS masterproject ON entr_project_project.entr_src_id = masterproject.project_id

         LEFT JOIN (SELECT epn.entr_src_id          AS parent_id,
                           child_part.part_id       AS c_part_id,
                           child_part.version_label AS c_version_label,
                           child_part.version_tree  AS c_version_tree,
                           child_part.name_1        AS c_packagename,
                           child_part.reference_1   AS c_packagecode,
                           child_part.weightmes_1   AS c_capacity,
                           child_part.weightmes_2   AS c_weight,
                           child_part.weightunit_1  AS c_capacityunit,
                           child_part.weightunit_2  AS c_weightunit,
                           child_part.active        AS c_active,
                           supplier.displayname     AS c_packagingsuppliername,
                           lgroup.entr_char1        AS c_packagingsuppliercode,
                           RESPONSIBLE.dir_id       AS c_dir_id,
                           RESPONSIBLE.displayname  AS c_displayname,
                           0                        AS c_child_total

                    FROM entr_part_nomclit epn
                             INNER JOIN nomclit n ON (
                            (
                                (
                                        nullif(nullif(n.ns, '*'), '') IS NULL
                                        OR split_part(n.ns, '/', 1) = 'EcoMundo'
                                    )
                                )
                            AND (epn.entr_dst_id = n.nomclit_id)
                        )
                             INNER JOIN entr_nomclit_part enp ON enp.entr_src_id = n.nomclit_id
                        AND enp.entr_tp = 'NOMENCLATURE_ITEM_FOR_PART_IS'

                             INNER JOIN part child_part ON enp.entr_dst_id = child_part.part_id

                             LEFT JOIN entr_part_dir lsup ON lsup.entr_src_id = child_part.part_id
                        AND lsup.entr_tp = 'SUPPLIER'
                             LEFT JOIN dir supplier ON supplier.dir_id = lsup.entr_dst_id
                             LEFT JOIN entr_dir_dir lgroup ON lgroup.entr_dst_id = supplier.dir_id
                        AND lgroup.entr_tp = 'SUPPLIER'
                             LEFT OUTER JOIN entr_part_dir RESPONSIBLE_LINK ON RESPONSIBLE_LINK.entr_src_id = part_id
                        AND RESPONSIBLE_LINK.entr_tp = 'RESPONSIBLE_PERSON'
                             LEFT OUTER JOIN dir RESPONSIBLE ON RESPONSIBLE.dir_id = RESPONSIBLE_LINK.entr_dst_id


                    WHERE epn.entr_tp = 'DIRECT_COMPO_PART'
                      AND (
                            (
                                (
                                        nullif(nullif(child_part.ns, '*'), '') IS NULL
                                        OR (
                                                child_part.acl_select IS NULL
                                                AND split_part(child_part.ns, '/', 1) = 'EcoMundo'
                                            )
                                        OR (
                                                    child_part.acl_select -> 'select' @> ANY (
                                                string_to_array('"RFUI-2107939","RFOI-543"', ',') :: JSONB[]
                                                )
                                            )
                                    )
                                )
                            AND (
                                        child_part.active IN (1)
                                    AND child_part.name_1 ILIKE '%%'
                                    AND child_part.tp = 'COSMETIC_PACKAGING'
                                )
                        )) child ON ((child.parent_id = mainPart.part_id))
    AND (
                                        (
                                                nullif(nullif(mainPart.ns, '*'), '') IS NULL
                                                OR (
                                                        mainPart.acl_select IS NULL
                                                        AND split_part(mainPart.ns, '/', 1) = 'EcoMundo'
                                                    )
                                                OR (
                                                            mainPart.acl_select -> 'select' @> ANY (
                                                        string_to_array('"RFUI-2107939","RFOI-543"', ',') :: JSONB[]
                                                        )
                                                    )
                                            )
                                        )

WHERE (
              (
                  (
                          nullif(nullif(mainPart.ns, '*'), '') IS NULL
                          OR (
                                  mainPart.acl_select IS NULL
                                  AND split_part(mainPart.ns, '/', 1) = 'EcoMundo'
                              )
                          OR (
                                      mainPart.acl_select -> 'select' @> ANY (
                                  string_to_array('"RFUI-2107939","RFOI-543"', ',') :: JSONB[]
                                  )
                              )
                      )
                  )
              AND (
                          mainPart.active IN (1)
                      AND (
                                      coalesce(mainPart.tp, '') ILIKE 'COSMETIC_PACKAGING'
                                  OR coalesce(mainPart.tp, '') ILIKE ''
                              )
                  )
          )

ORDER BY mainPart.part_id;



ORDER BY CAST(study_xml AS jsonb) -> 'Study result type'
;


WITH sb AS (SELECT cast(cast(study_xml AS jsonb) -> 'Study result type' AS text) des1
            FROM study
            WHERE study_xml IS NOT NULL)
SELECT *
FROM sb
ORDER BY 1;

CREATE TEMPORARY TABLE study_test
(
    xml jsonb NOT NULL DEFAULT '{}'
);

INSERT INTO study_test(xml)
SELECT cast(study_xml AS jsonb), js
FROM study;;

UPDATE doc SET ns = 'EcoMundo', st = 'VALID' WHERE doc_id = 481355;
