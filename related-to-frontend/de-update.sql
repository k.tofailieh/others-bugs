UPDATE de
SET de_xml = '{"data":[{"name":"CountryOfOrigin","label":"CountryOfOrigin","type":"list","path":"endptXml","value":[]},{"name":"AccessDate","label":"AccessDate","type":"text","path":"endptXml","value":""},{"name":"BiodiversitySpecies","label":"BiodiversitySpecies","type":"text","path":"endptXml","value":""}]}'
WHERE de_id = 64160;


SELECT sub_id, naml, s.type, inci_id, inciname, inci.tp, esi.entr_tp, inci.cas
FROM entr_subset_substances ess
         INNER JOIN substances s ON ess.entr_dst_id = s.sub_id
         INNER JOIN anx.entr_substances_inci esi ON esi.entr_src_id = ess.entr_dst_id
         INNER JOIN anx.inci ON esi.entr_dst_id = inci.inci_id
WHERE ess.entr_src_id = 4770