
/* original query */
select m_id, tp, m_ref, cas, marguage, container_de.de_ke as container
      from fiche
      left join der_fiche as container_der_fiche on container_der_fiche.der_src_id = m_id
      and container_der_fiche.der_tp = 'IN_CONTAINER'
      left join de as container_de on container_de.de_id = der_dst_id
      and container_de.de_tp = 'CONTAINER'
     where tp in ('RAW_MATERIAL', 'MASTER_RAW_MATERIAL') and active = 1

        and( (container_de.de_ke IS NULL AND m_attribute='0'))

         and m_id in (
        select entr_src_id
        from entr_fiche_endpt
        inner join endpt on entr_fiche_endpt.entr_dst_id = endpt.endpt_id and endpt.val between '0' and '1'
        where entr_fiche_endpt.entr_tp = 'PRICE')
      and m_id in (
        select entr_src_id from (select entr_src_id,
        case when endpt.val != ''
        then CAST(endpt.val as float) else 0.0 end as carbon
        from entr_fiche_endpt
        inner join endpt on entr_fiche_endpt.entr_dst_id = endpt.endpt_id
        where entr_fiche_endpt.entr_tp = 'RAW_MATERIAL_CARBON_FOOTPRINT') as all_footprint_carbon
        where all_footprint_carbon.carbon between 0.1 and 1.0)
      order by m_ref asc;

/* tests */
select  CASE when endpt.val ~ '^[0-9]+\.?[0-9]*$' then cast(endpt.val as float) else 0 end from endpt;

select  cast(val as float)  from endpt where endpt.val ~ '^[0-9]+\.?[0-9]*$';

select distinct val from endpt;


/* modified query. */

select entr_src_id, carbon from (select entr_src_id,
case when endpt.val  ~ '^[0-9]+\.?[0-9]*$'
then CAST(endpt.val as float) else 0 end as carbon
from entr_fiche_endpt
inner join endpt on entr_fiche_endpt.entr_dst_id = endpt.endpt_id
where entr_fiche_endpt.entr_tp = 'RAW_MATERIAL_CARBON_FOOTPRINT') as all_footprint_carbon
where carbon between '0' and '10'
ORDER BY carbon DESC;

